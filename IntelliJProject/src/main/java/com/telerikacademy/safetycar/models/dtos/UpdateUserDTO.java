package com.telerikacademy.safetycar.models.dtos;

import lombok.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import static com.telerikacademy.safetycar.utils.Constants.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UpdateUserDTO {

    private int id;

    @Size(min = MIN_USER_FIRST_NAME_LENGTH,
            max = MAX_USER_FIRST_NAME_LENGTH,
            message = USER_FIRST_NAME_LENGTH_ERROR_MESSAGE)
    @Pattern(regexp = NAME_FORMAT, message = NAME_FORMAT_ERROR_MSG)
    private String firstName;

    @Size(min = MIN_USER_LAST_NAME_LENGTH,
            max = MAX_USER_LAST_NAME_LENGTH,
            message = USER_LAST_NAME_LENGTH_ERROR_MESSAGE)
    @Pattern(regexp = NAME_FORMAT, message = NAME_FORMAT_ERROR_MSG)
    private String lastName;

    @Pattern(regexp = PHONE_FORMAT, message = PHONE_FORMAT_ERROR_MSG)
    @Size(max = POLICY_HOLDER_PHONE_MAX_LENGTH)
    private String phone;

    private MultipartFile picture;

    private String oldPictureUrl;
}