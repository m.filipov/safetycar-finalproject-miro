package com.telerikacademy.safetycar.models;

import com.opencsv.bean.CsvBindByPosition;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MulticriteriaCase {

    @CsvBindByPosition(position = 0)
    private String ccMin;

    @CsvBindByPosition(position = 1)
    private String ccMax;

    @CsvBindByPosition(position = 2)
    private String carAgeMin;

    @CsvBindByPosition(position = 3)
    private String carAgeMax;

    @CsvBindByPosition(position = 4)
    private String baseAmount;

    @Override
    public String toString() {
        return "MulticriteriaCase{" +
                "ccMin='" + ccMin + '\'' +
                ", ccMax='" + ccMax + '\'' +
                ", carAgeMin='" + carAgeMin + '\'' +
                ", carAgeMax='" + carAgeMax + '\'' +
                ", baseAmount='" + baseAmount + '\'' +
                '}';
    }
}