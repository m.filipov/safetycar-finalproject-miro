package com.telerikacademy.safetycar.models;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "policy_requests")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PolicyRequest {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "start_date")
    private String startDate;

    @Column(name = "end_date")
    private String endDate;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "phone")
    private String phone;

    @Column(name = "email")
    private String email;

    @ManyToOne
    @JoinColumn(name = "city_id")
    private City city;

    @Column(name = "postal_address")
    private String postalAddress;

    @Column(name = "vehicle_reg_certificate_image")
    private String vehicleRegistrationImage;

    @Column(name = "request_date")
    private String requestDate;

    @OneToOne
    @JoinColumn(name = "simulation_offer_id")
    private SimulationOffer simulationOffer;

    @ManyToOne
    @JoinColumn(name = "owner_id")
    private UserInfo owner;

    @ManyToOne
    @JoinColumn(name = "status_id")
    private Status status;

    @Column(name = "deleted")
    private boolean deleted;

    @Column(name = "request_number")
    private int requestNumber;
}