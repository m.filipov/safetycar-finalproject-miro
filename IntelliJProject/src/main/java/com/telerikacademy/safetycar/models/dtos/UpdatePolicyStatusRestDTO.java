package com.telerikacademy.safetycar.models.dtos;

import lombok.*;

import javax.validation.constraints.Positive;

import static com.telerikacademy.safetycar.utils.Constants.ID_SHOULD_BE_POSITIVE;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UpdatePolicyStatusRestDTO {

    @Positive(message = ID_SHOULD_BE_POSITIVE)
    private int id;

    private String status;
}