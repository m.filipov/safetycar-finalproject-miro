package com.telerikacademy.safetycar.models.dtos;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ShowUsersDTO {

    private String firstName;

    private String lastName;

    private String email;

    private String phone;
}