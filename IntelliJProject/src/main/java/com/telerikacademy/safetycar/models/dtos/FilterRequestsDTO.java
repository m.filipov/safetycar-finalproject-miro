package com.telerikacademy.safetycar.models.dtos;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class FilterRequestsDTO {

    public Integer status;
    public String  creatorEmail;
    public String  holderEmail;
    public Integer requestNumber;
    public String  sortParam;
    public String  orderParam;
}