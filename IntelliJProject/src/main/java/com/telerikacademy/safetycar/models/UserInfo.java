package com.telerikacademy.safetycar.models;

import lombok.*;
import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.Size;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "user_info")
@SQLDelete(sql = "UPDATE user_details SET deleted = '1' WHERE id = ?", check = ResultCheckStyle.COUNT)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "email")
    private String email;

    @Column(name = "phone")
    private String phone;

    @Column(name = "deleted")
    private Boolean deleted;

    @Column(name = "picture")
    private String picture;

    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "id")
    private Set<Secret> secrets;

    public UserInfo(int id,
                    @Size(min = 2, max = 20, message = "Invalid input, the first name should be between 2 and 20 characters") String firstName,
                    @Size(min = 2, max = 20, message = "Invalid input, the last name should be between 2 and 20 characters") String lastName,
                    @Email(regexp = "^(.+)@(.+)$") String email,
                    String phone,
                    Boolean deleted) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.phone = phone;
        this.deleted = deleted;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserInfo userInfo = (UserInfo) o;
        return id == userInfo.id &&
                Objects.equals(firstName, userInfo.firstName) &&
                Objects.equals(lastName, userInfo.lastName) &&
                Objects.equals(email, userInfo.email) &&
                Objects.equals(phone, userInfo.phone) &&
                Objects.equals(deleted, userInfo.deleted) &&
                Objects.equals(picture, userInfo.picture);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, firstName, lastName, email, phone, deleted, picture);
    }
}