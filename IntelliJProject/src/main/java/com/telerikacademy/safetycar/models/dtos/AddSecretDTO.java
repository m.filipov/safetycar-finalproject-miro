package com.telerikacademy.safetycar.models.dtos;

import lombok.*;

import javax.validation.constraints.Size;

import static com.telerikacademy.safetycar.utils.Constants.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AddSecretDTO {

    private int userId;
    private int questionId;
    private String questionName;

    @Size(min = ANSWER_MIN_LENGTH, max = ANSWER_MAX_LENGTH, message = ANSWER_ERROR_MESSAGE)
    private String answer;
}