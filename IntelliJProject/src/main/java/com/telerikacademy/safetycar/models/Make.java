package com.telerikacademy.safetycar.models;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "makes")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Make {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "name")
    private String name;
}