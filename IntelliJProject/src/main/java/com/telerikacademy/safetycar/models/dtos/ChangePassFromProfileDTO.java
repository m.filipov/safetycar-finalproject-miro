package com.telerikacademy.safetycar.models.dtos;

import lombok.*;

import javax.validation.constraints.Pattern;

import static com.telerikacademy.safetycar.utils.Constants.PASSWORD_PATTERN;
import static com.telerikacademy.safetycar.utils.Constants.USER_PASSWORD_ERROR_MESSAGE;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ChangePassFromProfileDTO {

    private int userId;

    private String oldPass;

    @Pattern(regexp = PASSWORD_PATTERN,
            message = USER_PASSWORD_ERROR_MESSAGE)
    private String newPass;

    private String confirmNewPass;
}