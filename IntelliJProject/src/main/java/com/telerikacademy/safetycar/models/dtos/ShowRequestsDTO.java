package com.telerikacademy.safetycar.models.dtos;

import lombok.*;
import org.springframework.web.multipart.MultipartFile;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder(access = AccessLevel.PUBLIC)
public class ShowRequestsDTO {

    private String firstName;

    private String lastName;

    private String startDate;

    private String endDate;

    private String phone;

    private String email;

    private int city;

    private String address;

    private MultipartFile vehicleRegistrationImage;

    private String requestDate;

    private int simulationOffer;

    private int ownerId;

    private String status;

    private int requestNumber;
}