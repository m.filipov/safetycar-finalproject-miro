package com.telerikacademy.safetycar.models.dtos;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.Email;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

import static com.telerikacademy.safetycar.utils.Constants.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CreatePolicyRequestDTO {

    @JsonFormat(pattern = DATE_FORMAT)
    private String startDate;

    @Size(min = MIN_POLICY_HOLDER_FIRST_NAME_LENGTH,
            max = MAX_POLICY_HOLDER_FIRST_NAME_LENGTH,
            message = POLICY_HOLDER_FIRST_NAME_ERROR_MESSAGE)
    @Pattern(regexp = NAME_FORMAT, message = NAME_FORMAT_ERROR_MSG)
    private String firstName;

    @Size(min = MIN_POLICY_HOLDER_LAST_NAME_LENGTH,
            max = MAX_POLICY_HOLDER_LAST_NAME_LENGTH,
            message = POLICY_HOLDER_LAST_NAME_ERROR_MESSAGE)
    @Pattern(regexp = NAME_FORMAT, message = NAME_FORMAT_ERROR_MSG)
    private String lastName;

    @Pattern(regexp = PHONE_FORMAT, message = PHONE_FORMAT_ERROR_MSG)
    @Size(max = POLICY_HOLDER_PHONE_MAX_LENGTH)
    private String phone;

    @Email(regexp = EMAIL_FORMAT, message = EMAIL_FORMAT_ERROR_MESSAGE)
    private String email;

    @Positive(message = CITY_ERROR)
    private int cityId;

    @Size(min = MIN_POLICY_HOLDER_POSTAL_ADDRESS_LENGTH,
            max = MAX_POLICY_HOLDER_POSTAL_ADDRESS_LENGTH,
            message = POLICY_HOLDER_POSTAL_ADDRESS_LENGTH_ERROR_MESSAGE)
    private String postalAddress;

    private MultipartFile vehicleRegistrationImage;
}