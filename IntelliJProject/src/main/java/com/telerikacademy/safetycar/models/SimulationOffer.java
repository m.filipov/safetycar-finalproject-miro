package com.telerikacademy.safetycar.models;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "simulation_offers")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SimulationOffer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "cubic_capacity")
    private int cubicCapacity;

    @Column(name = "first_registration_date")
    private String firstRegistrationDate;

    @Column(name = "driver_age")
    private int driverAge;

    @Column(name = "prev_year_accidents")
    private boolean prevYearAccidents;

    @Column(name = "premium")
    private double premium;

    @ManyToOne
    @JoinColumn(name = "model_id")
    private Model model;

    public boolean getPrevYearAccidents() {
        return prevYearAccidents;
    }

    public void setPrevYearAccidents(boolean prevYearAccidents) {
        this.prevYearAccidents = prevYearAccidents;
    }
}