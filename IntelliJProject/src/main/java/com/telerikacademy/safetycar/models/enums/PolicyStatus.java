package com.telerikacademy.safetycar.models.enums;

public enum PolicyStatus {

    PENDING("pending"),
    APPROVED("approved"),
    REJECTED("rejected"),
    CANCELLED("cancelled");

    private final String str;

    PolicyStatus(String str) {
        this.str = str;
    }

    public String getString() {
        return str;
    }
}