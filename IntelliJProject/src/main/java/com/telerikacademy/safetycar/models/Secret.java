package com.telerikacademy.safetycar.models;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "secrets")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Secret {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @OneToOne(targetEntity = UserInfo.class, fetch = FetchType.EAGER)
    @JoinColumn(nullable = false, name = "user_id")
    private UserInfo user;

    @ManyToOne
    @JoinColumn(name = "question_id")
    private Question question;

    @Column
    private String answer;

    public Secret(UserInfo user, Question question, String answer) {
        this.user = user;
        this.question = question;
        this.answer = answer;
    }
}