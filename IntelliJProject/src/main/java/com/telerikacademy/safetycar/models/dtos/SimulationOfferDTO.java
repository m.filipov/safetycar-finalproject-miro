package com.telerikacademy.safetycar.models.dtos;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;
import org.springframework.format.annotation.NumberFormat;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;

import static com.telerikacademy.safetycar.utils.Constants.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SimulationOfferDTO {

    @NumberFormat
    @PositiveOrZero
    @Max(MAX_CAR_CUBIC_CAPACITY)
    private int cubicCapacity;

    @JsonFormat(pattern = DATE_FORMAT)
    private String firstRegistrationDate;

    @Min(MIN_DRIVER_AGE)
    private int driverAge;

    private boolean prevYearAccidents;

    @NotNull(message = CAR_MAKE_ERROR_MESSAGE)
    @PositiveOrZero(message = CAR_MAKE_ERROR_MESSAGE)
    private Integer makeId;

    @NotNull(message = CAR_MODEL_ERROR_MESSAGE)
    @PositiveOrZero(message = CAR_MAKE_ERROR_MESSAGE)
    private Integer modelId;

    @PositiveOrZero
    private double premium;

    public boolean getPrevYearAccidents() {
        return prevYearAccidents;
    }

    public void setPrevYearAccidents(boolean prevYearAccidents) {
        this.prevYearAccidents = prevYearAccidents;
    }
}