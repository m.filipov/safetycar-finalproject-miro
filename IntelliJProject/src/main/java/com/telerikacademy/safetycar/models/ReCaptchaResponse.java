package com.telerikacademy.safetycar.models;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ReCaptchaResponse {
    private boolean success;
    private String  challenge_ts;
    private String  hostName;
}