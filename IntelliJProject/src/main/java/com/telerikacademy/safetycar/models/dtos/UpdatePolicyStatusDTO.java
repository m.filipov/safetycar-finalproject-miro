package com.telerikacademy.safetycar.models.dtos;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UpdatePolicyStatusDTO {

    private String status;
}