package com.telerikacademy.safetycar.models.dtos;

import lombok.*;

import javax.validation.constraints.Email;
import javax.validation.constraints.Size;

import static com.telerikacademy.safetycar.utils.Constants.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ContactDTO {

    @Size(min = EMAIL_NAME_MIN_LENGTH, max = EMAIL_NAME_MAX_LENGTH, message = CONTACT_EMAIL_NAME_MESSAGE)
    private String name;

    @Email(regexp = EMAIL_FORMAT, message = CONTACT_EMAIL_FORMAT)
    private String email;

    @Size(min = SUBJECT_MIN_LENGTH, message = SUBJECT_ERROR_MESSAGE)
    private String subject;

    @Size(min = CONTACT_EMAIL_MESSAGE_MIN, max = CONTACT_EMAIL_MESSAGE_MAX, message = CONTACT_EMAIL_MESSAGE_ERROR)
    private String message;
}