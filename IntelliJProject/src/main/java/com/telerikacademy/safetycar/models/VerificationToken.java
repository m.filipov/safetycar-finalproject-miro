package com.telerikacademy.safetycar.models;

import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "verification_token")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class VerificationToken {
    private static final int EXPIRATION = 60 * 24;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @OneToOne(targetEntity = User.class, fetch = FetchType.EAGER)
    @JoinColumn(nullable = false, name = "username")
    private User user;

    @Column(name = "token")
    private String token;

    @Column(name = "expiry_date")
    private Date expiry;

    public VerificationToken(User user, String token) {
        this.user = user;
        this.token = token;
    }
}