package com.telerikacademy.safetycar.repositories.impl;

import com.telerikacademy.safetycar.exceptions.EntityNotFoundException;
import com.telerikacademy.safetycar.models.Model;
import com.telerikacademy.safetycar.repositories.contracts.ModelRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.telerikacademy.safetycar.repositories.RepositoriesConstants.Model.*;

@Repository
public class ModelRepositoryImpl implements ModelRepository {
    SessionFactory sessionFactory;

    @Autowired
    public ModelRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


    @Override
    public List<Model> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Model> query = session.createQuery(GET_ALL_FROM_MODEL, Model.class);
            return query.list();
        }
    }

    @Override
    public Model getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Model model = session.get(Model.class, id);
            if (model == null) {
                throw new EntityNotFoundException(
                        String.format(MODEL_NOT_FOUND_MESSAGE, id));
            }
            return model;
        }
    }

    @Override
    public List<Model> getByMakeId(int makeId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Model> query = session.createQuery(GET_FROM_MODEL_BY_MAKE, Model.class);
            query.setParameter(1, makeId);
            return query.list();
        }
    }
}