package com.telerikacademy.safetycar.repositories.impl;

import com.telerikacademy.safetycar.models.Authority;
import com.telerikacademy.safetycar.repositories.contracts.AuthorityRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class AuthorityRepositoryImpl implements AuthorityRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public AuthorityRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void create(Authority authority) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(authority);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(Authority authority) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(authority);
            session.getTransaction().commit();
        }
    }
}