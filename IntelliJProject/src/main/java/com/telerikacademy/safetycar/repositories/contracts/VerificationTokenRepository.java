package com.telerikacademy.safetycar.repositories.contracts;

import com.telerikacademy.safetycar.models.User;
import com.telerikacademy.safetycar.models.VerificationToken;

public interface VerificationTokenRepository {

    VerificationToken findByToken(String token);

    VerificationToken findByUser(User user);

    void save(VerificationToken verificationToken);

}