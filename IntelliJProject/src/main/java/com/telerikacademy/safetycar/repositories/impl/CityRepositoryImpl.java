package com.telerikacademy.safetycar.repositories.impl;

import com.telerikacademy.safetycar.exceptions.EntityNotFoundException;
import com.telerikacademy.safetycar.models.City;
import com.telerikacademy.safetycar.repositories.contracts.CityRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.telerikacademy.safetycar.repositories.RepositoriesConstants.City.FROM_CITY;
import static com.telerikacademy.safetycar.utils.Constants.CITY_WITH_ID_D_NOT_FOUND;

@Repository
public class CityRepositoryImpl implements CityRepository {
    SessionFactory sessionFactory;

    @Autowired
    public CityRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<City> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<City> query = session.createQuery(FROM_CITY, City.class);
            return query.list();
        }
    }

    @Override
    public City getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            City city = session.get(City.class, id);
            checkIfCityExists(id, city);
            return city;
        }
    }

    private void checkIfCityExists(int id, City city) {
        if (city == null) {
            throw new EntityNotFoundException(
                    String.format(CITY_WITH_ID_D_NOT_FOUND, id));
        }
    }
}