package com.telerikacademy.safetycar.repositories.impl;

import com.telerikacademy.safetycar.exceptions.EntityNotFoundException;
import com.telerikacademy.safetycar.models.Status;
import com.telerikacademy.safetycar.repositories.contracts.StatusRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.telerikacademy.safetycar.repositories.RepositoriesConstants.Status.*;

@Repository
public class StatusRepositoryImpl implements StatusRepository {
    SessionFactory sessionFactory;

    @Autowired
    public StatusRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Status> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Status> query = session.createQuery(GET_ALL_FROM_STATUS, Status.class);
            return query.list();
        }
    }

    @Override
    public Status getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Status status = session.get(Status.class, id);
            if (status == null) {
                throw new EntityNotFoundException(
                        String.format(STATUS_WITH_ID_NOT_FOUND_MESSAGE, id));
            }
            return status;
        }
    }

    @Override
    public Status getByStatus(String statusName) {
        try (Session session = sessionFactory.openSession()) {
            Query<Status> query = session.createQuery(GET_BY_STATUS_NAME, Status.class);
            query.setParameter(STATUS_NAME, statusName);
            List<Status> statuses = query.list();
            Status status = statuses.get(0);
            if (status == null) {
                throw new EntityNotFoundException(
                        String.format(STATUS_NOT_FOUND_MESSAGE, statusName));
            }
            return status;
        }
    }
}