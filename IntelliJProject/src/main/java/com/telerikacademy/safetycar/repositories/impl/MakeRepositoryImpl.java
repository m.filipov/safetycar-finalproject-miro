package com.telerikacademy.safetycar.repositories.impl;

import com.telerikacademy.safetycar.exceptions.EntityNotFoundException;
import com.telerikacademy.safetycar.models.Make;
import com.telerikacademy.safetycar.repositories.contracts.MakeRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.telerikacademy.safetycar.repositories.RepositoriesConstants.Make.GET_ALL_FROM_MAKE_ORDERED_BY_ID;
import static com.telerikacademy.safetycar.repositories.RepositoriesConstants.Make.MAKE_ID_NOT_FOUND_MESSAGE;

@Repository
public class MakeRepositoryImpl implements MakeRepository {
    SessionFactory sessionFactory;

    @Autowired
    public MakeRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Make> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Make> query = session.createQuery(GET_ALL_FROM_MAKE_ORDERED_BY_ID, Make.class);
            return query.list();
        }
    }

    @Override
    public Make getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Make make = session.get(Make.class, id);
            if (make == null) {
                throw new EntityNotFoundException(
                        String.format(MAKE_ID_NOT_FOUND_MESSAGE, id));
            }
            return make;
        }
    }
}