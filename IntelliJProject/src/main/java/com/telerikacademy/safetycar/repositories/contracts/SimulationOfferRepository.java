package com.telerikacademy.safetycar.repositories.contracts;

import com.telerikacademy.safetycar.models.SimulationOffer;

import java.util.List;

public interface SimulationOfferRepository {

    List<SimulationOffer> getAll();

    SimulationOffer getById(int id);

    void create(SimulationOffer simulationOffer);

    double getBaseAmountFromDbTable(SimulationOffer simulationOffer);

    double getBaseAmountFromCsv(SimulationOffer simulationOffer);

    void update(SimulationOffer simulationOffer);

}