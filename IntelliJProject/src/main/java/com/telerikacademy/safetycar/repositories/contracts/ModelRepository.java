package com.telerikacademy.safetycar.repositories.contracts;

import com.telerikacademy.safetycar.models.Model;

import java.util.List;

public interface ModelRepository {

    List<Model> getAll();

    Model getById(int id);

    List<Model> getByMakeId(int makeId);

}