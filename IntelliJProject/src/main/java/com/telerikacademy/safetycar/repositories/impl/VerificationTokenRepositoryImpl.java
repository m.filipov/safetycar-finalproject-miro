package com.telerikacademy.safetycar.repositories.impl;

import com.telerikacademy.safetycar.exceptions.EntityNotFoundException;
import com.telerikacademy.safetycar.models.User;
import com.telerikacademy.safetycar.models.VerificationToken;
import com.telerikacademy.safetycar.repositories.contracts.VerificationTokenRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.telerikacademy.safetycar.repositories.RepositoriesConstants.Token.*;
import static com.telerikacademy.safetycar.utils.Constants.MISSING_TOKEN_ERROR_MESSAGE;

@Repository
public class VerificationTokenRepositoryImpl implements VerificationTokenRepository {
    SessionFactory sessionFactory;

    @Autowired
    public VerificationTokenRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public VerificationToken findByToken(String token) {
        try (Session session = sessionFactory.openSession()) {
            Query<VerificationToken> query = session.createQuery(FIND_TOKEN, VerificationToken.class);
            query.setParameter(TOKEN, token);
            List<VerificationToken> resultList = query.getResultList();
            checkIfTokenIsMissing(resultList);
            return resultList.get(0);
        }
    }

    @Override
    public VerificationToken findByUser(User user) {
        try (Session session = sessionFactory.openSession()) {
            Query<VerificationToken> query = session.createQuery(FIND_TOKEN_BY_USER, VerificationToken.class);
            query.setParameter(USER, user);
            List<VerificationToken> resultList = query.getResultList();
            checkIfTokenIsMissing(resultList);
            return resultList.get(0);
        }
    }

    @Override
    public void save(VerificationToken verificationToken) {
        try (Session session = sessionFactory.openSession()) {
            session.save(verificationToken);
        }
    }

    private void checkIfTokenIsMissing(List<VerificationToken> resultList) {
        if (resultList.isEmpty()) {
            throw new EntityNotFoundException(MISSING_TOKEN_ERROR_MESSAGE);
        }
    }
}