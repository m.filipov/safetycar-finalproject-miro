package com.telerikacademy.safetycar.repositories.contracts;

import com.telerikacademy.safetycar.models.PolicyRequest;

import java.util.List;
import java.util.Optional;

public interface PolicyRequestRepository {

    List<PolicyRequest> getAll();

    PolicyRequest getById(int id);

    List<PolicyRequest> getByCreator(int userId);

    void create(PolicyRequest policyRequest);

    List<PolicyRequest> filter(Optional<Integer> status,
                               Optional<String> creatorEmail,
                               Optional<String> holderEmail,
                               Optional<Integer> requestNumber,
                               Optional<String> sortParam,
                               String orderParam);

    void update(PolicyRequest policyRequest);

}