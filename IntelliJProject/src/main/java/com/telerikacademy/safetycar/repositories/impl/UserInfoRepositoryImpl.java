package com.telerikacademy.safetycar.repositories.impl;

import com.telerikacademy.safetycar.exceptions.EntityNotFoundException;
import com.telerikacademy.safetycar.models.UserInfo;
import com.telerikacademy.safetycar.repositories.contracts.UserInfoRepository;
import com.telerikacademy.safetycar.utils.HelperMethods;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.telerikacademy.safetycar.repositories.RepositoriesConstants.User.*;

@Repository
public class UserInfoRepositoryImpl implements UserInfoRepository {
    private final SessionFactory sessionFactory;

    public UserInfoRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void create(UserInfo userInfo) {
        try (Session session = sessionFactory.openSession()) {
            session.save(userInfo);
        }
    }

    @Override
    public List<UserInfo> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<UserInfo> query = session.createQuery(GET_ALL_ACTIVE_USER_INFO, UserInfo.class);
            return query.list();
        }
    }

    @Override
    public UserInfo getById(int userInfoId) {
        try (Session session = sessionFactory.openSession()) {
            UserInfo userInfo = session.get(UserInfo.class, userInfoId);
            return userInfo;
        }
    }

    @Override
    public void update(UserInfo userInfo) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(userInfo);
            session.getTransaction().commit();
        }
    }

    @Override
    public UserInfo getByEmail(String email) {
        try (Session session = sessionFactory.openSession()) {
            Query<UserInfo> query = session.createQuery(GET_USER_INFO_BY_EMAIL, UserInfo.class);
            query.setParameter(EMAIL, email);
            List<UserInfo> users = query.list();
            if (users.isEmpty()) {
                throw new EntityNotFoundException(
                        String.format(USER_INFO_NOT_FOUND_MESSAGE, email));
            }
            return users.get(0);
        }
    }

    @Override
    public boolean existsByEmail(UserInfo userInfo) {
        try (Session session = sessionFactory.openSession()) {
            Set<String> currentEmails =
                    new HashSet<>(HelperMethods.getSingleColumnFromUserInfoAsStringsQuery(session, userInfo.getId(), "email"));
            return currentEmails.contains(userInfo.getEmail());
        }
    }
}