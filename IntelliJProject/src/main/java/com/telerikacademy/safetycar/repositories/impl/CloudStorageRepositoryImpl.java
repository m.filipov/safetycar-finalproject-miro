package com.telerikacademy.safetycar.repositories.impl;

import com.cloudinary.Cloudinary;
import com.cloudinary.Transformation;
import com.cloudinary.utils.ObjectUtils;
import com.telerikacademy.safetycar.repositories.contracts.CloudStorageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.util.Map;

import static com.telerikacademy.safetycar.repositories.RepositoriesConstants.Cloudinary.*;

@Repository
public class CloudStorageRepositoryImpl implements CloudStorageRepository {
    private final Cloudinary cloudinary;

    @Autowired
    public CloudStorageRepositoryImpl(Cloudinary cloudinary) {
        this.cloudinary = cloudinary;
    }

    @Override
    public String uploadFile(byte[] bytes, String fileType, int width) throws IOException {
        Map uploadResult = cloudinary
                .uploader()
                .upload(bytes, ObjectUtils.asMap(TRANSFORMATION,
                        new Transformation()
                                .crop(LIMIT)
                                .width(width),
                        RESOURCE_TYPE, fileType));
        return uploadResult.get(URL).toString();
    }
}