package com.telerikacademy.safetycar.repositories.impl;

import com.telerikacademy.safetycar.exceptions.EntityNotFoundException;
import com.telerikacademy.safetycar.models.Question;
import com.telerikacademy.safetycar.repositories.contracts.QuestionRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.telerikacademy.safetycar.repositories.RepositoriesConstants.Secret.GET_ALL_FROM_QUESTION;
import static com.telerikacademy.safetycar.utils.Constants.QUESTION_NOT_FOUND;

@Repository
public class QuestionRepositoryImpl implements QuestionRepository {
    SessionFactory sessionFactory;

    @Autowired
    public QuestionRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Question> findAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Question> query = session.createQuery(GET_ALL_FROM_QUESTION, Question.class);
            return query.list();
        }
    }

    @Override
    public Question findById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Question question = session.get(Question.class, id);
            checkIfQuestionExists(id, question);
            return question;
        }
    }

    private void checkIfQuestionExists(int id, Question question) {
        if (question == null) {
            throw new EntityNotFoundException(
                    String.format(QUESTION_NOT_FOUND, id));
        }
    }
}