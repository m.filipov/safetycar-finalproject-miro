package com.telerikacademy.safetycar.repositories.contracts;

import com.telerikacademy.safetycar.models.Question;

import java.util.List;

public interface QuestionRepository {

    List<Question> findAll();

    Question findById(int id);

}