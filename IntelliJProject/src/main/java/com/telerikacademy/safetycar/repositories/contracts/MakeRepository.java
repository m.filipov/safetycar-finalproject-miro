package com.telerikacademy.safetycar.repositories.contracts;

import com.telerikacademy.safetycar.models.Make;

import java.util.List;

public interface MakeRepository {

    List<Make> getAll();

    Make getById(int id);

}