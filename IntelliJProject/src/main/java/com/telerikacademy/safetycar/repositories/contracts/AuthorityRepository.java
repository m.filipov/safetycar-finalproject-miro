package com.telerikacademy.safetycar.repositories.contracts;

import com.telerikacademy.safetycar.models.Authority;

public interface AuthorityRepository {

    void create(Authority authority);

    void delete(Authority authority);

}