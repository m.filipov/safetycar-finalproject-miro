package com.telerikacademy.safetycar.repositories.contracts;

import com.telerikacademy.safetycar.models.UserInfo;

import java.util.List;

public interface UserInfoRepository {

    void create(UserInfo userInfo);

    List<UserInfo> getAll();

    UserInfo getById(int userId);

    boolean existsByEmail(UserInfo user);

    void update(UserInfo userInfo);

    UserInfo getByEmail(String email);

}