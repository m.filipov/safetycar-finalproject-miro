package com.telerikacademy.safetycar.repositories.contracts;

import java.io.IOException;

public interface CloudStorageRepository {

    String uploadFile(byte[] bytes, String fileType, int width) throws IOException;

}