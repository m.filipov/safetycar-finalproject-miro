package com.telerikacademy.safetycar.repositories.contracts;

import com.telerikacademy.safetycar.models.Secret;
import com.telerikacademy.safetycar.models.User;

import java.util.List;

public interface SecretRepository {

    Secret findByAnswer(String answer);

    List<Secret> findByUserId(int userId);

    List<Secret> findByUser(User user);

    void save(Secret secret);

}