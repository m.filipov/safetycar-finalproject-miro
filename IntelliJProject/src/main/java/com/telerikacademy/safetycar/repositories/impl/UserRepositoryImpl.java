package com.telerikacademy.safetycar.repositories.impl;

import com.telerikacademy.safetycar.exceptions.EntityNotFoundException;
import com.telerikacademy.safetycar.models.User;
import com.telerikacademy.safetycar.repositories.contracts.UserRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.telerikacademy.safetycar.repositories.RepositoriesConstants.User.*;

@Repository
public class UserRepositoryImpl implements UserRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<User> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery(GET_ALL_USERS, User.class);
            return query.list();
        }
    }

    @Override
    public void update(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(user);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            user.getUserInfo().setDeleted(true);
            user.setEnabled(false);
            session.update(user);
            session.getTransaction().commit();
        }
    }

    @Override
    public User getByEmail(String email) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery(GET_BY_USERNAME, User.class);
            query.setParameter("username", email);
            List<User> users = query.list();
            if (users.isEmpty()) {
                throw new EntityNotFoundException(
                        String.format(USER_NOT_FOUND_MESSAGE, email));
            }
            return users.get(0);
        }
    }
}