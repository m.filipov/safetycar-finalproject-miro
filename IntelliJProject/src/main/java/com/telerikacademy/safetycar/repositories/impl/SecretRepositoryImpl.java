package com.telerikacademy.safetycar.repositories.impl;

import com.telerikacademy.safetycar.exceptions.EntityNotFoundException;
import com.telerikacademy.safetycar.models.Secret;
import com.telerikacademy.safetycar.models.User;
import com.telerikacademy.safetycar.repositories.contracts.SecretRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.telerikacademy.safetycar.repositories.RepositoriesConstants.Secret.*;
import static com.telerikacademy.safetycar.utils.Constants.ANSWER_NOT_FOUND;

@Repository
public class SecretRepositoryImpl implements SecretRepository {
    SessionFactory sessionFactory;

    @Autowired
    public SecretRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Secret findByAnswer(String answer) {
        try (Session session = sessionFactory.openSession()) {
            Query<Secret> query = session.createQuery(FIND_BY_ANSWER, Secret.class);
            query.setParameter(ANSWER, answer);
            List<Secret> resultList = query.getResultList();
            checkIfAnswerExists(resultList);
            return resultList.get(0);
        }
    }

    @Override
    public List<Secret> findByUserId(int userId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Secret> query = session.createQuery(FIND_BY_USER_ID, Secret.class);
            query.setParameter(USER_ID, userId);
            return query.list();
        }
    }

    @Override
    public List<Secret> findByUser(User user) {
        try (Session session = sessionFactory.openSession()) {
            Query<Secret> query = session.createQuery(FIND_BY_USER, Secret.class);
            query.setParameter(USER, user);
            return query.list();
        }
    }

    @Override
    public void save(Secret secret) {
        try (Session session = sessionFactory.openSession()) {
            session.save(secret);
        }
    }

    private void checkIfAnswerExists(List<Secret> resultList) {
        if (resultList.isEmpty()) {
            throw new EntityNotFoundException(ANSWER_NOT_FOUND);
        }
    }
}