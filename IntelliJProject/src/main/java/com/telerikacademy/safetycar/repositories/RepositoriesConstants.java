package com.telerikacademy.safetycar.repositories;

public interface RepositoriesConstants {

    final class City {
        public static final String FROM_CITY = "from City";
    }

    final class Cloudinary {
        public static final String TRANSFORMATION = "transformation";
        public static final String LIMIT          = "limit";
        public static final String RESOURCE_TYPE  = "resource_type";
        public static final String URL            = "url";
    }

    final class Make {
        public static final String GET_ALL_FROM_MAKE_ORDERED_BY_ID = "from Make order by id";
        public static final String MAKE_ID_NOT_FOUND_MESSAGE = "Make with ID %d not found!";
    }

    final class Model {
        public static final String GET_ALL_FROM_MODEL      = "from Model";
        public static final String MODEL_NOT_FOUND_MESSAGE = "Car model with ID %d not found!";
        public static final String GET_FROM_MODEL_BY_MAKE  = "from Model where make.id = ?1";
    }

    final class PolicyRequest {
        public static final String GET_ALL_FROM_POLICIES_WITH_STATUS = "from PolicyRequest where status.id != 4 order by requestNumber desc";
        public static final String POLICY_WITH_ID_NOT_FOUND_MESSAGE  = "Policy with id %d not found!";
        public static final String GET_POLICY_BY_CREATOR             = "from PolicyRequest where owner.id = :owner order by requestNumber desc ";
        public static final String OWNER                             = "owner";
        public static final String STATUS                            = "status";
        public static final String EMAIL                             = "email";
        public static final String REQUEST_NUMBER                    = "requestNumber";
        public static final String DELETED                           = "deleted";
        public static final String DESCENDING                        = "Descending";

        public static final String STATUS_LABEL         = "Status";
        public static final String START_DATE_LABEL     = "StartDate";
        public static final String REQUEST_DATE_LABEL   = "RequestDate";
        public static final String REQUEST_NUMBER_LABEL = "RequestNumber";
        public static final String PREMIUM_LABEL        = "Premium";
        public static final String START_DATE           = "startDate";
        public static final String REQUEST_DATE         = "requestDate";
        public static final String SIMULATION_OFFER     = "simulationOffer";
        public static final String PREMIUM              = "premium";
    }

    final class Secret {
        public static final String GET_ALL_FROM_QUESTION = "from Question";
        public static final String FIND_BY_ANSWER        = "from Secret where question = :answer";
        public static final String ANSWER                = "answer";
        public static final String FIND_BY_USER_ID       = "from Secret where user_id = :userId";
        public static final String USER_ID               = "userId";
        public static final String FIND_BY_USER          = "from Secret where user = :user";
        public static final String USER                  = "user";
    }

    final class SimulationOffer {
        public static final String GET_ALL_FROM_SIMULATION_OFFER = "from SimulationOffer";
        public static final String OFFER_NOT_FOUND_MESSAGE       = "Offer with ID %d not found!";
        public static final String GET_BASE_AMOUNT               = "select mc.base_amount from safety_car.multicriteria_calculator mc where ?1 >= mc.cc_min and ?1 <= cc_max and ?2 >= car_age_min and ?2 <= car_age_max";
    }

    final class Status {
        public static final String GET_ALL_FROM_STATUS              = "from Status order by id";
        public static final String STATUS_WITH_ID_NOT_FOUND_MESSAGE = "Status with ID %d not found!";
        public static final String GET_BY_STATUS_NAME               = "from Status where statusName = :statusName";
        public static final String STATUS_NAME                      = "statusName";
        public static final String STATUS_NOT_FOUND_MESSAGE         = "Status %s not found!";
    }

    final class User {
        public static final String GET_ALL_ACTIVE_USER_INFO    = "from UserInfo where deleted = false";
        public static final String GET_USER_INFO_BY_EMAIL      = "from UserInfo where email = :email";
        public static final String EMAIL                       = "email";
        public static final String USER_INFO_NOT_FOUND_MESSAGE = "UserInfo with email %s not found!";

        public static final String GET_ALL_USERS          = "from User";
        public static final String GET_BY_USERNAME        = "from User where username = :username";
        public static final String USER_NOT_FOUND_MESSAGE = "User with username (email) %s not found!";
    }

    final class Token {
        public static final String FIND_TOKEN         = "from VerificationToken where token = :token";
        public static final String TOKEN              = "token";
        public static final String FIND_TOKEN_BY_USER = "from VerificationToken where user = :user";
        public static final String USER               = "user";
    }
}