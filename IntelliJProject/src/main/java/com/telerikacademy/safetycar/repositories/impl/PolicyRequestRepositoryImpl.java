package com.telerikacademy.safetycar.repositories.impl;

import com.telerikacademy.safetycar.exceptions.EntityNotFoundException;
import com.telerikacademy.safetycar.models.PolicyRequest;
import com.telerikacademy.safetycar.repositories.contracts.PolicyRequestRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.telerikacademy.safetycar.repositories.RepositoriesConstants.PolicyRequest.*;

@Repository
public class PolicyRequestRepositoryImpl implements PolicyRequestRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public PolicyRequestRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<PolicyRequest> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<PolicyRequest> query = session.createQuery(GET_ALL_FROM_POLICIES_WITH_STATUS, PolicyRequest.class);
            return query.list();
        }
    }

    @Override
    public PolicyRequest getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            PolicyRequest policyRequest = session.get(PolicyRequest.class, id);
            if (policyRequest == null) {
                throw new EntityNotFoundException(
                        String.format(POLICY_WITH_ID_NOT_FOUND_MESSAGE, id));
            }
            return policyRequest;
        }
    }

    @Override
    public List<PolicyRequest> getByCreator(int userId) {
        try (Session session = sessionFactory.openSession()) {
            Query<PolicyRequest> query = session.createQuery(GET_POLICY_BY_CREATOR, PolicyRequest.class);
            query.setParameter(OWNER, userId);
            return query.list();
        }
    }

    @Override
    public void create(PolicyRequest policyRequest) {
        try (Session session = sessionFactory.openSession()) {
            session.save(policyRequest);
        }
    }

    @Override
    public List<PolicyRequest> filter(Optional<Integer> status,
                                      Optional<String> creatorEmail,
                                      Optional<String> holderEmail,
                                      Optional<Integer> requestNumber,
                                      Optional<String> sortParam,
                                      String orderParam) {
        List<PolicyRequest> filteredPolicies = new ArrayList<>();
        if (status.isEmpty() && creatorEmail.isEmpty() &&
                holderEmail.isEmpty() && requestNumber.isEmpty()) {
            return new ArrayList<>();
        }
        try (Session session = sessionFactory.openSession()) {
            CriteriaBuilder cb = session.getCriteriaBuilder();
            CriteriaQuery<PolicyRequest> cq = cb.createQuery(PolicyRequest.class);
            Root<PolicyRequest> policyRoot = cq.from(PolicyRequest.class);
            List<Predicate> predicates = new ArrayList<>();

            if (status.isPresent() && status.get() != 0) {
                predicates.add(cb.equal(policyRoot.get(STATUS), status.get()));
            }

            if (creatorEmail.isPresent() && !creatorEmail.get().equals("")) {
                predicates.add(cb.like(policyRoot.get(OWNER).get(EMAIL), "%" + creatorEmail.get() + "%"));
            }

            if (holderEmail.isPresent() && !holderEmail.get().equals("")) {
                predicates.add(cb.like(policyRoot.get(EMAIL), "%" + holderEmail.get() + "%"));
            }

            if (requestNumber.isPresent() && requestNumber.get() != 0) {
                predicates.add(cb.equal(policyRoot.get(REQUEST_NUMBER), requestNumber.get()));
            }

            predicates.add(cb.equal(policyRoot.get(DELETED), false));
            cq.select(policyRoot).distinct(true).where(predicates.toArray(Predicate[]::new));

            if (sortParam.isPresent()) {
                Path<PolicyRequest> policyPath;
                switch (sortParam.get()) {
                    case STATUS_LABEL:
                        policyPath = policyRoot.get(STATUS);
                        getAscOrDesc(orderParam, cb, cq, policyPath);
                        break;
                    case START_DATE_LABEL:
                        policyPath = policyRoot.get(START_DATE);
                        getAscOrDesc(orderParam, cb, cq, policyPath);
                        break;
                    case REQUEST_DATE_LABEL:
                        policyPath = policyRoot.get(REQUEST_DATE);
                        getAscOrDesc(orderParam, cb, cq, policyPath);
                        break;
                    case REQUEST_NUMBER_LABEL:
                        policyPath = policyRoot.get(REQUEST_NUMBER);
                        getAscOrDesc(orderParam, cb, cq, policyPath);
                        break;
                    case PREMIUM_LABEL:
                        policyPath = policyRoot.get(SIMULATION_OFFER).get(PREMIUM);
                        getAscOrDesc(orderParam, cb, cq, policyPath);
                        break;
                }
            }
            filteredPolicies = session.createQuery(cq).getResultList();
            return filteredPolicies;
        }
    }

    @Override
    public void update(PolicyRequest policyRequest) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(policyRequest);
            session.getTransaction().commit();
        }
    }

    private void getAscOrDesc(String orderParam, CriteriaBuilder cb, CriteriaQuery<PolicyRequest> cq, Path<PolicyRequest> policyPath) {
        if (orderParam.equals(DESCENDING)) {
            cq.orderBy(cb.desc(policyPath));
        } else {
            cq.orderBy(cb.asc(policyPath));
        }
    }
}