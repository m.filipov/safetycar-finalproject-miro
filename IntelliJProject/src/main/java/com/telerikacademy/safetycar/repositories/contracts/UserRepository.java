package com.telerikacademy.safetycar.repositories.contracts;

import com.telerikacademy.safetycar.models.User;

import java.util.List;

public interface UserRepository {

    List<User> getAll();

    void update(User user);

    void delete(User user);

    User getByEmail(String email);

}