package com.telerikacademy.safetycar.repositories.impl;

import com.telerikacademy.safetycar.exceptions.EntityNotFoundException;
import com.telerikacademy.safetycar.exceptions.InvalidInputException;
import com.telerikacademy.safetycar.models.MulticriteriaCase;
import com.telerikacademy.safetycar.models.SimulationOffer;
import com.telerikacademy.safetycar.repositories.contracts.SimulationOfferRepository;
import com.telerikacademy.safetycar.utils.CsvParserService;
import com.telerikacademy.safetycar.utils.DatesHelperMethods;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

import static com.telerikacademy.safetycar.repositories.RepositoriesConstants.SimulationOffer.*;
import static com.telerikacademy.safetycar.utils.Constants.*;

@Repository
public class SimulationOfferRepositoryImpl implements SimulationOfferRepository {
    SessionFactory sessionFactory;

    @Autowired
    public SimulationOfferRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<SimulationOffer> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<SimulationOffer> query = session.createQuery(GET_ALL_FROM_SIMULATION_OFFER, SimulationOffer.class);
            return query.list();
        }
    }

    @Override
    public SimulationOffer getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            SimulationOffer simulationOffer = session.get(SimulationOffer.class, id);
            if (simulationOffer == null) {
                throw new EntityNotFoundException(
                        String.format(OFFER_NOT_FOUND_MESSAGE, id));
            }
            return simulationOffer;
        }
    }

    @Override
    public void create(SimulationOffer simulationOffer) {
        try (Session session = sessionFactory.openSession()) {
            session.save(simulationOffer);
        }
    }

    private Integer getCarAge(String registrationDate) {
        Date regDate = DatesHelperMethods.parseDate(registrationDate);
        int age = DatesHelperMethods.getAge(regDate);
        if (age < 0) {
            throw new InvalidInputException(CAR_REGISTRATION_DATE_ERROR);
        }
        return age;
    }

    public double getBaseAmountFromDbTable(SimulationOffer simulationOffer) {
        try (Session session = sessionFactory.openSession()) {
            Query<Double> query = session.createNativeQuery(GET_BASE_AMOUNT);
            query.setParameter(1, simulationOffer.getCubicCapacity());
            query.setParameter(2, getCarAge(simulationOffer.getFirstRegistrationDate()));
            if (query.getResultList().isEmpty()) {
                throw new EntityNotFoundException(CALCULATE_PREMIUM_ERROR_MESSAGE);
            }
            return query.getResultList().get(0);
        }
    }

    @Override
    public double getBaseAmountFromCsv(SimulationOffer simulationOffer) {
        List<MulticriteriaCase> rows = CsvParserService.parseCsv(MULTICRITERIA_CSV_PATH);
        int cubicCapacity = simulationOffer.getCubicCapacity();
        int carAge = getCarAge(simulationOffer.getFirstRegistrationDate());
        double result = 0;

        for (int i = 1, rowsSize = rows.size(); i < rowsSize; i++) {
            MulticriteriaCase bean = rows.get(i);

            if (cubicCapacity >= Integer.parseInt(bean.getCcMin()) && cubicCapacity <= Integer.parseInt(bean.getCcMax()) &&
                    carAge >= Integer.parseInt(bean.getCarAgeMin()) && carAge <= Integer.parseInt(bean.getCarAgeMax())) {

                result = Double.parseDouble(bean.getBaseAmount());
                break;
            }
        }
        return result;
    }

    @Override
    public void update(SimulationOffer simulationOffer) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(simulationOffer);
            session.getTransaction().commit();
        }
    }
}