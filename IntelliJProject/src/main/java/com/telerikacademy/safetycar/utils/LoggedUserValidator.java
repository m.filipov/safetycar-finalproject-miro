package com.telerikacademy.safetycar.utils;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import java.security.Principal;

import static com.telerikacademy.safetycar.utils.Constants.NOT_LOGGED_IN_ERROR_MSG;

public class LoggedUserValidator {

    public static void checkIfUserIsLogged(Principal principal) {
        if (principal == null) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, NOT_LOGGED_IN_ERROR_MSG);
        }
    }
}