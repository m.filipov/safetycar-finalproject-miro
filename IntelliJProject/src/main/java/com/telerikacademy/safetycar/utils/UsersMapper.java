package com.telerikacademy.safetycar.utils;

import com.telerikacademy.safetycar.models.Question;
import com.telerikacademy.safetycar.models.Secret;
import com.telerikacademy.safetycar.models.User;
import com.telerikacademy.safetycar.models.UserInfo;
import com.telerikacademy.safetycar.models.dtos.*;
import com.telerikacademy.safetycar.services.contracts.FileService;
import com.telerikacademy.safetycar.services.contracts.SecretService;
import com.telerikacademy.safetycar.services.contracts.UserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.telerikacademy.safetycar.utils.Constants.DEFAULT_PROFILE_PICTURE;

@Component
public class UsersMapper {
    private final PasswordEncoder passwordEncoder;
    private final UserInfoService userInfoService;
    private final FileService fileService;
    private final SecretService secretService;

    @Autowired
    public UsersMapper(PasswordEncoder passwordEncoder, UserInfoService userInfoService, FileService fileService, SecretService secretService) {
        this.passwordEncoder = passwordEncoder;
        this.userInfoService = userInfoService;
        this.fileService = fileService;
        this.secretService = secretService;
    }

    public UserInfo userInfoFromDto(UserRegistrationDTO dto) {
        UserInfo userInfo = new UserInfo();
        userInfo.setEmail(dto.getEmail());
        userInfo.setFirstName(dto.getFirstName());
        userInfo.setLastName(dto.getLastName());

        MultipartFile picture = dto.getPicture();
        if (!picture.isEmpty()) {
            fileService.getFileType(picture);
            String pictureUrl = fileService.uploadFile(picture, 500);
            if (pictureUrl != null) {
                userInfo.setPicture(pictureUrl);
            }
        } else {
            userInfo.setPicture(DEFAULT_PROFILE_PICTURE);
        }
        userInfo.setPhone(dto.getPhone());
        userInfoService.create(userInfo);
        userInfo.setDeleted(true);
        userInfoService.update(userInfo);
        return userInfo;
    }

    public ShowUsersDTO getShowUsersDto(UserInfo userInfo) {
        ShowUsersDTO dto = new ShowUsersDTO();
        dto.setFirstName(userInfo.getFirstName());
        dto.setLastName(userInfo.getLastName());
        dto.setEmail(userInfo.getEmail());
        dto.setPhone(userInfo.getPhone());
        return dto;
    }

    public List<ShowUsersDTO> getShowAllUsers(List<UserInfo> userInfos) {
        return userInfos.stream().map(this::getShowUsersDto).collect(Collectors.toList());
    }

    public org.springframework.security.core.userdetails.User securityUserFromDto(UserRegistrationDTO dto) {

        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_USER");
        String encodedPassword = passwordEncoder.encode(dto.getPassword());

        org.springframework.security.core.userdetails.User newUser =
                new org.springframework.security.core.userdetails.User(
                        dto.getEmail(), encodedPassword, false, true, true, true, authorities);
        return newUser;
    }

    public UpdateUserDTO toDto(int id) {
        UserInfo userInfo = userInfoService.getById(id);
        UpdateUserDTO dto = new UpdateUserDTO();
        dto.setFirstName(userInfo.getFirstName());
        dto.setLastName(userInfo.getLastName());
        dto.setPhone(userInfo.getPhone());
        return dto;
    }

    public List<EditSecretDto> toSecretDto(int userId) {
        List<EditSecretDto> result = new ArrayList<>();

        List<Secret> secrets = secretService.findByUserId(userId);
        for (Secret secret : secrets) {
            EditSecretDto dto = new EditSecretDto();

            Question question = secret.getQuestion();
            int questionId = question.getId();
            String questionName = question.getName();
            String answer = secret.getAnswer();

            dto.setQuestionId(questionId);
            dto.setQuestionName(questionName);
            dto.setAnswer(answer);
            dto.setUserId(userId);

            result.add(dto);
        }
        return result;
    }

    public UpdateUserDTO showUserProfileDto(String email) {
        UpdateUserDTO dto = new UpdateUserDTO();
        UserInfo userInfo = userInfoService.getByEmail(email);
        dto.setFirstName(userInfo.getFirstName());
        dto.setLastName(userInfo.getLastName());
        dto.setPhone(userInfo.getPhone());
        dto.setOldPictureUrl(userInfo.getPicture());
        dto.setId(userInfo.getId());
        return dto;
    }

    public User fromDtoChangePass(User user, ChangePasswordDTO dto) {
        String encodedPassword = passwordEncoder.encode(dto.getPassword());
        user.setPassword(encodedPassword);
        return user;
    }
}