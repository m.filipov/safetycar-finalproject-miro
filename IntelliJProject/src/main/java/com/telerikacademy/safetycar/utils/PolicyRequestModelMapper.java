package com.telerikacademy.safetycar.utils;

import com.telerikacademy.safetycar.models.*;
import com.telerikacademy.safetycar.models.dtos.*;
import com.telerikacademy.safetycar.models.enums.PolicyStatus;
import com.telerikacademy.safetycar.services.contracts.CityService;
import com.telerikacademy.safetycar.services.contracts.FileService;
import com.telerikacademy.safetycar.services.contracts.StatusService;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static com.telerikacademy.safetycar.utils.DatesHelperMethods.calculateEndDate;

@Component
public class PolicyRequestModelMapper {
    private final CityService cityService;
    private final StatusService statusService;
    private final FileService fileService;

    public PolicyRequestModelMapper(CityService cityService, StatusService statusService, FileService fileService) {
        this.cityService = cityService;
        this.statusService = statusService;
        this.fileService = fileService;
    }

    public PolicyRequest getPolicyRequestFromDTO(CreatePolicyRequestDTO createPolicyRequestDTO,
                                                 UserInfo loggedUserInfo) {
        PolicyRequest policyRequest = new PolicyRequest();
        String startDate = createPolicyRequestDTO.getStartDate();
        DatesHelperMethods.checkIfDateInTheFuture(startDate);
        policyRequest.setStartDate(startDate);
        policyRequest.setEndDate(calculateEndDate(createPolicyRequestDTO.getStartDate()));
        policyRequest.setFirstName(createPolicyRequestDTO.getFirstName());
        policyRequest.setLastName(createPolicyRequestDTO.getLastName());
        policyRequest.setPhone(createPolicyRequestDTO.getPhone());
        policyRequest.setEmail(createPolicyRequestDTO.getEmail());
        City city = cityService.getById(createPolicyRequestDTO.getCityId());
        policyRequest.setCity(city);
        policyRequest.setPostalAddress(createPolicyRequestDTO.getPostalAddress());

        MultipartFile picture = createPolicyRequestDTO.getVehicleRegistrationImage();
        fileService.getFileType(picture);
        String pictureUrl = fileService.uploadFile(picture, 600);
        policyRequest.setVehicleRegistrationImage(pictureUrl);

        Status status = statusService.getByStatusName(PolicyStatus.PENDING.getString());
        policyRequest.setStatus(status);
        policyRequest.setDeleted(false);
        Date now = new Date();
        SimpleDateFormat DATE_FORMAT = new SimpleDateFormat(Constants.DATE_FORMAT);
        String date = DATE_FORMAT.format(now);
        policyRequest.setRequestDate(date);
        policyRequest.setOwner(loggedUserInfo);
        return policyRequest;

    }

    public ShowRequestsDTO getShowRequestsDto(PolicyRequest policyRequest) {
        ShowRequestsDTO dto = new ShowRequestsDTO();
        dto.setFirstName(policyRequest.getFirstName());
        dto.setLastName(policyRequest.getLastName());
        dto.setStartDate(policyRequest.getStartDate());
        dto.setEndDate(policyRequest.getEndDate());
        dto.setPhone(policyRequest.getPhone());
        dto.setEmail(policyRequest.getEmail());
        dto.setCity(policyRequest.getCity().getId());
        dto.setAddress(policyRequest.getPostalAddress());
        dto.setRequestDate(policyRequest.getRequestDate());
        dto.setSimulationOffer(policyRequest.getSimulationOffer().getId());
        dto.setOwnerId(policyRequest.getOwner().getId());
        dto.setStatus(policyRequest.getStatus().getStatusName());
        dto.setRequestNumber(policyRequest.getRequestNumber());
        return dto;
    }

    public List<ShowRequestsDTO> getShowAll(List<PolicyRequest> policyRequests) {
        return policyRequests.stream().map(this::getShowRequestsDto).collect(Collectors.toList());
    }
}