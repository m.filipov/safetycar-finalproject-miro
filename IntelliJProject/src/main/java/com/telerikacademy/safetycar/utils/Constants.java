package com.telerikacademy.safetycar.utils;

public class Constants {

    // General input constraints
    public static final String DATE_FORMAT                = "yyyy-MM-dd";
    public static final String EMAIL_FORMAT               = "^(.+)@(.+)$";
    public static final String EMAIL_FORMAT_ERROR_MESSAGE = "Email should be in the following format user@domain.com!";
    public static final String NAME_FORMAT                = "^[A-Za-z][A-Za-z-.\\s]*$";
    public static final String NAME_FORMAT_ERROR_MSG      = "The name can include only letters, - and space";
    public static final String PHONE_FORMAT               = "^[0-9+][0-9]*$";
    public static final String PHONE_FORMAT_ERROR_MSG     = "The phone can include only numbers";
    public static final String GENERAL_ERROR_MESSAGE      = "An error occurred. Please try again or contact support.";

    // Policy Request creation constants (CreatePolicyRequestDTO)
    public static final int    MIN_POLICY_HOLDER_FIRST_NAME_LENGTH               = 2;
    public static final int    MAX_POLICY_HOLDER_FIRST_NAME_LENGTH               = 20;
    public static final String POLICY_HOLDER_FIRST_NAME_ERROR_MESSAGE            = "The first name should be between " + MIN_POLICY_HOLDER_FIRST_NAME_LENGTH + " and " + MAX_POLICY_HOLDER_FIRST_NAME_LENGTH + " characters";
    public static final int    MIN_POLICY_HOLDER_LAST_NAME_LENGTH                = 2;
    public static final int    MAX_POLICY_HOLDER_LAST_NAME_LENGTH                = 20;
    public static final String POLICY_HOLDER_LAST_NAME_ERROR_MESSAGE             = "The last name should be between " + MIN_POLICY_HOLDER_LAST_NAME_LENGTH + " and " + MAX_POLICY_HOLDER_LAST_NAME_LENGTH + " characters";
    public static final int    MIN_POLICY_HOLDER_POSTAL_ADDRESS_LENGTH           = 2;
    public static final int    MAX_POLICY_HOLDER_POSTAL_ADDRESS_LENGTH           = 500;
    public static final String POLICY_HOLDER_POSTAL_ADDRESS_LENGTH_ERROR_MESSAGE = "The postal address should be between " + MIN_POLICY_HOLDER_POSTAL_ADDRESS_LENGTH + " and " + MAX_POLICY_HOLDER_POSTAL_ADDRESS_LENGTH + " characters";
    public static final int    POLICY_HOLDER_PHONE_MAX_LENGTH                    = 20;
    public static final String CITY_ERROR                                        = "Please select a city";

    // Policy Request errors
    public static final String NOT_AUTHORIZED_TO_APPROVE_DENY_POLICY_REQUEST_ERROR_MSG = "You are not authorized to approve/deny this policy request.";
    public static final String NOT_AUTHORIZED_TO_CANCEL_POLICY_REQUEST_ERROR_MSG       = "You are not authorized to cancel this policy request. Only the owner can cancel his policy request.";
    public static final String ID_SHOULD_BE_POSITIVE                                   = "The id should be positive";

    // Simulation Offer Calculation constraints
    public static final double ACCIDENT_COEFF                  = 1.2;
    public static final double TAX_AMOUNT                      = 0.1;
    /**
     * This is the age after which the DRIVER_AGE_COEEFF will be added on top of the cost of the Premium
     */
    public static final int    BASE_DRIVER_AGE                 = 25;
    public static final double DRIVER_AGE_COEFF                = 1.05;
    public static final int    REQUEST_NUMBER                  = 1000;
    public static final String CALCULATE_PREMIUM_ERROR_MESSAGE = "Invalid input parameters. Cannot calculate premium.";
    public static final String CAR_REGISTRATION_DATE_ERROR     = "Car registration date cannot be in the future.";

    // Simulation Offer creation constants (SimulationOfferDTO)
    public static final int    MAX_CAR_CUBIC_CAPACITY  = 999999;
    public static final int    MIN_DRIVER_AGE          = 18;
    public static final String CAR_MAKE_ERROR_MESSAGE  = "The make cannot be empty. Please select make!";
    public static final String CAR_MODEL_ERROR_MESSAGE = "The model cannot be empty. Please select model!";

    // User registration constraints
    public static final int    MIN_USER_FIRST_NAME_LENGTH               = 2;
    public static final int    MAX_USER_FIRST_NAME_LENGTH               = 20;
    public static final String USER_FIRST_NAME_LENGTH_ERROR_MESSAGE     = "The first name should be between " + MIN_USER_FIRST_NAME_LENGTH + " and " + MAX_USER_FIRST_NAME_LENGTH + " characters long";
    public static final int    MIN_USER_LAST_NAME_LENGTH                = 2;
    public static final int    MAX_USER_LAST_NAME_LENGTH                = 20;
    public static final String USER_LAST_NAME_LENGTH_ERROR_MESSAGE      = "The last name should be between " + MIN_USER_LAST_NAME_LENGTH + " and " + MAX_USER_LAST_NAME_LENGTH + " characters long";
    public static final String PASSWORD_PATTERN                         = "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^*-]).{8,}$";
    public static final String USER_PASSWORD_ERROR_MESSAGE              = "The password should be minimum 8 characters, with at least one uppercase letter, one lowercase letter, one number and one special character(#?!@$%^*-)";
    public static final String VERIFICATION_TOKEN_EXPIRED_ERROR_MESSAGE = "Your verification token has expired!";
    public static final String ACCOUNT_ACTIVATED_MESSAGE                = "Your account has been activated successfully!";
    public static final String DEFAULT_PROFILE_PICTURE                  = "http://res.cloudinary.com/dopjcnzjt/image/upload/v1605115307/wq5hhgxf1qnyp2uvd0rj.png";
    public static final String USER_WITH_THIS_EMAIL_NOT_FOUND           = "User with this email not found!";

    // Other User messages
    public static final String USER_WITH_THIS_ID_EXISTS_ERROR_MESSAGE   = "User with this Id already exists.";
    public static final String NOT_LOGGED_IN_ERROR_MSG                  = "You must log in in order to proceed!";
    public static final String NOT_AUTHORIZED_TO_MODIFY_USERS_ERROR_MSG = "You are not authorized to modify users";
    public static final String WRONG_PASSWORD_ENTERED                   = "Wrong password entered";
    public static final String OLD_PASS_SAME_AS_NEW_ONE                 = "The new password should be different from the old one!";
    public static final String USER_NOT_AN_ADMIN                        = "This user is not an admin!";
    public static final String GET_ALL_CAR_MAKES                        = "Get car make by its ID";

    // Authorities
    public static final String ROLE_ADMIN         = "ROLE_ADMIN";
    public static final String AGENT_ACCESS_ERROR = "You can't grant Agent access to this user!";

    // Verification Token error message
    public static final String MISSING_TOKEN_ERROR_MESSAGE = "Missing token";

    // Other
    public static final String EMAIL_TAKEN_ERROR_MESSAGE                 = "Cannot proceed, this email is already taken.";
    public static final String NOT_ALLOWED_TO_EDIT_USERS_ERROR_MESSAGE   = "You are not allowed to edit users!";
    public static final String NOT_ALLOWED_TO_DELETE_USERS_ERROR_MESSAGE = "You are not allowed to delete users!";
    public static final String CITY_WITH_ID_D_NOT_FOUND                  = "City with id %d not found!";
    public static final String QUESTION_NOT_FOUND                        = "Question with id %d not found!";

    public static final String THE_PASSWORDS_DON_T_MATCH           = "The passwords entered don't match!";
    public static final String USER_WITH_THIS_EMAIL_ALREADY_EXISTS = "User with this email already exists!";
    public static final String PLEASE_VERIFY_NOT_A_ROBOT           = "Please verify, that you are not a robot!";

    public static final int    EXPIRY_TIME_NEXT_DAY = 24 * 60;
    public static final String START_DATE_ERROR_MSG = "The start date cannot be in the past.";
    public static final double MILLISECONDS_IN_YEAR = 3.15576e+10;

    // Swagger method descriptions
    public static final String CALCULATE_AND_SHOW_PREMIUM_FOR_SIMULATION_OFFER = "Calculate and show the total premium in BGN for a simulation offer";

    public static final String SHOW_ALL_REQUESTS_OF_LOGGED_USER = "Show all requests created by the logged user";
    public static final String CANCEL_POLICY_REQUEST            = "Cancel a policy request";

    public static final String FILTER_POLICY_REQUESTS   = "Filter policy requests by different criteria";
    public static final String CREATE_FULL_POLICY       = "Create full policy";
    public static final String SHOW_ALL_POLICY_REQUESTS = "Show all policy requests";

    public static final String SHOW_ALL_REQUESTS_IN_THE_SYSTEM = "Show all requests in the system";
    public static final String GET_POLICY_REQUEST_BY_ID        = "Get a policy request by its ID";
    public static final String APPROVE_OR_DENY_POLICY_REQUEST  = "Approve or deny a policy request";

    public static final String LIST_ALL_CAR_MAKES = "List of all car makes";

    public static final String LIST_ALL_CAR_MODELS       = "List of all car models";
    public static final String GET_CAR_MODEL_BY_ID       = "Get car model by ID";
    public static final String GET_CAR_MODELS_BY_MAKE_ID = "Get all car models from a specific make (inputting the make ID)";

    public static final String LIST_ALL_CITIES = "List of all cities";
    public static final String GET_CITY_BY_ID  = "Get city by its ID";

    public static final String SHOW_ALL_USERS = "Show all users";
    public static final String GET_USER_BY_ID = "Get user by ID";
    public static final String UPDATE_USER    = "Update a user";

    // Email
    public static final String SAFETY_CAR_EMAIL_ADDRESS    = "safety.car.insurance.team7@gmail.com";
    public static final int    EMAIL_NAME_MIN_LENGTH       = 2;
    public static final int    EMAIL_NAME_MAX_LENGTH       = 20;
    public static final String CONTACT_EMAIL_NAME_MESSAGE  = "Name should be between 2 and 20 characters!";
    public static final String CONTACT_EMAIL_MESSAGE       = "The message cannot be empty!";
    public static final int    CONTACT_EMAIL_MESSAGE_MIN   = 5;
    public static final int    CONTACT_EMAIL_MESSAGE_MAX   = 400;
    public static final String CONTACT_EMAIL_MESSAGE_ERROR = "The message should be between 5 and 400 characters long!";
    public static final String CONTACT_EMAIL_FORMAT        = "Please enter a valid email!";
    public static final String SUBJECT_ERROR_MESSAGE       = "Subject should be at least 2 characters long!";
    public static final int    SUBJECT_MIN_LENGTH          = 2;
    public static final String TEST_EMAIL                  = "test@test.com";
    public static final String VERIFY_EMAIL_MESSAGE        = "Verify your email address";

    // ReCaptcha
    public static final String RECAPTCHA_URL      = "https://www.google.com/recaptcha/api/siteverify";
    public static final String RECAPTCHA_RESPONSE = "?secret=6LedBtgZAAAAABuyDzH0jBm5FxYMP7pSNrA7vfJw&response=";

    // Secret questions
    public static final String ANSWER_NOT_FOUND     = "Answer not found!";
    public static final int    ANSWER_MIN_LENGTH    = 2;
    public static final int    ANSWER_MAX_LENGTH    = 50;
    public static final String ANSWER_ERROR_MESSAGE = "The answer should be between " + ANSWER_MIN_LENGTH + " and " + ANSWER_MAX_LENGTH + " characters long";

    // Paths
    public static final String MULTICRITERIA_CSV_PATH = "src/main/resources/multicriteria/multicriteria.csv";
}