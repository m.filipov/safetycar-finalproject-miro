package com.telerikacademy.safetycar.utils;

import com.opencsv.bean.CsvToBeanBuilder;
import com.telerikacademy.safetycar.models.MulticriteriaCase;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class CsvParserService {

    public static List<MulticriteriaCase> parseCsv(String file) {
        List<MulticriteriaCase> beans = new ArrayList<>();
        try {
            beans = new CsvToBeanBuilder(new FileReader(file))
                    .withType(MulticriteriaCase.class)
                    .build()
                    .parse();
        } catch (FileNotFoundException e) {
            throw new com.telerikacademy.safetycar.exceptions.FileNotFoundException(e.getMessage());
        }
        return beans;
    }
}