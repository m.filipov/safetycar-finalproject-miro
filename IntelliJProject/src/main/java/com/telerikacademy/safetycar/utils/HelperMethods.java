package com.telerikacademy.safetycar.utils;

import com.telerikacademy.safetycar.models.UserInfo;
import org.hibernate.Session;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

public class HelperMethods {

    public static List<String> getSingleColumnFromUserInfoAsStringsQuery(Session session, int currentUserId, String fieldName) {
        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<String> stringCriteriaQuery = criteriaBuilder.createQuery(String.class);
        Root<UserInfo> root = stringCriteriaQuery.from(UserInfo.class);
        List<Predicate> predicates = new ArrayList<>();
        // where id != currentId
        predicates.add(criteriaBuilder.notEqual(root.get("id"), currentUserId));
        predicates.add(criteriaBuilder.equal(root.get("deleted"), false));
        Selection<String> selection = stringCriteriaQuery.select(root.get(fieldName)).where(predicates.toArray(Predicate[]::new)).getSelection();
        // Selection Alias
        CompoundSelection<String> projection = criteriaBuilder.construct(String.class, selection);
        stringCriteriaQuery.select(projection);
        return session.createQuery(stringCriteriaQuery).getResultList();
    }
}