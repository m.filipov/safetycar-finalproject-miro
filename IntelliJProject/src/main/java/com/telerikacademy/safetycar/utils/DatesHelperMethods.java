package com.telerikacademy.safetycar.utils;

import com.telerikacademy.safetycar.exceptions.InvalidInputException;
import com.telerikacademy.safetycar.exceptions.UnauthorizedOperationException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static com.telerikacademy.safetycar.utils.Constants.*;

public class DatesHelperMethods {

    public static Date parseDate(String inputDate) {
        SimpleDateFormat ft = new SimpleDateFormat(DATE_FORMAT);
        Date date;
        try {
            date = ft.parse(inputDate);
            return date;
        } catch (ParseException e) {
            throw new UnauthorizedOperationException(e.getMessage());
        }
    }

    public static Integer getAge(Date date) {
        Date now = new Date();
        long timeBetween = now.getTime() - date.getTime();
        double yearsBetween = timeBetween / MILLISECONDS_IN_YEAR;
        int age = (int) Math.floor(yearsBetween);
        return age;
    }


    public static void checkIfDateInTheFuture(String startDate) {
        Date checkStartDate = parseDate(startDate);
        Date now = new Date();
        long timeBetween = checkStartDate.getTime() - now.getTime();
        if (timeBetween < 0) {
            throw new InvalidInputException(START_DATE_ERROR_MSG);
        }
    }

    public static String calculateEndDate(String startDate) {
        String startDateString = startDate.substring(0, 4);
        int startDateInt = Integer.parseInt(startDateString);
        int endDateInt = startDateInt + 1;
        String endDate = String.valueOf(endDateInt);
        String lastSixChars = startDate.substring(4, 10);
        return endDate + lastSixChars;
    }
}