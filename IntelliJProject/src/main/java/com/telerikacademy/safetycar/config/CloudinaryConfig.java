package com.telerikacademy.safetycar.config;

import com.cloudinary.Cloudinary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

import java.util.HashMap;
import java.util.Map;

import static com.telerikacademy.safetycar.config.ConfigConstants.Cloudinary.*;

@Configuration
@PropertySource("classpath:application.properties")
public class CloudinaryConfig {
    private final String cloudName;
    private final String apiKey;
    private final String apiSecret;

    @Autowired
    public CloudinaryConfig(Environment env) {
        cloudName = env.getProperty(CLOUDINARY_CLOUD_NAME);
        apiKey = env.getProperty(CLOUDINARY_API_KEY);
        apiSecret = env.getProperty(CLOUDINARY_API_SECRET);
    }

    @Bean
    public Cloudinary cloudinary() {
        Map<String, String> config = new HashMap<>();
        config.put(CLOUD_NAME, cloudName);
        config.put(API_KEY, apiKey);
        config.put(API_SECRET, apiSecret);
        return new Cloudinary(config);
    }
}