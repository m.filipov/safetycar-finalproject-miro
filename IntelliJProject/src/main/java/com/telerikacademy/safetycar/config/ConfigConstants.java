package com.telerikacademy.safetycar.config;

public interface ConfigConstants {

    final class Cloudinary {
        public static final String CLOUDINARY = "cloudinary";

        public static final String CLOUD_NAME = "cloud_name";
        public static final String API_KEY    = "api_key";
        public static final String API_SECRET = "api_secret";

        public static final String CLOUDINARY_CLOUD_NAME = CLOUDINARY + "." + CLOUD_NAME;
        public static final String CLOUDINARY_API_KEY    = CLOUDINARY + "." + API_KEY;
        public static final String CLOUDINARY_API_SECRET = CLOUDINARY + "." + API_SECRET;
    }

    final class Hibernate {
        public static final String DATABASE = "database";
        public static final String URL      = DATABASE + "." + "url";
        public static final String USERNAME = DATABASE + "." + "username";
        public static final String PASSWORD = DATABASE + "." + "password";

        public static final String MODELS            = "com.telerikacademy.safetycar.models";
        public static final String JDBC_DRIVER       = "com.mysql.cj.jdbc.Driver";
        public static final String HIBERNATE_DIALECT = "hibernate.dialect";
        public static final String MYSQL_DIALECT     = "org.hibernate.dialect.MySQLDialect";
    }

    final class Swagger {
        public static final String REST_CONTROLLERS = "com.telerikacademy.safetycar.controllers.rest";
        public static final String ERROR_PATH       = "/error.*";
        public static final String API_NAME         = "Safety Car REST API";
        public static final String API_DESCRIPTION  = "Application for car insurance";
        public static final String API_VERSION      = "1.0.0";
    }

    final class Twilio {
        public static final String TWILIO          = "twilio";

        public static final String SID             = TWILIO + "." + "account_sid";
        public static final String TOKEN           = TWILIO + "." + "auth_token";
        public static final String TRIAL_NUMBER    = TWILIO + "." + "trial_number";

        public static final String SUCCESS_MESSAGE = "Twilio initialized successfully!";
    }
}