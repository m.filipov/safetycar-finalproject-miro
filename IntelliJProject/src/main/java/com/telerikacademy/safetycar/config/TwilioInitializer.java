package com.telerikacademy.safetycar.config;

import com.twilio.sdk.Twilio;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import static com.telerikacademy.safetycar.config.ConfigConstants.Twilio.SUCCESS_MESSAGE;

@Configuration
public class TwilioInitializer {
    private final static Logger              LOGGER = LoggerFactory.getLogger(TwilioInitializer.class);
    private final        TwilioConfiguration twilioConfiguration;

    @Autowired
    public TwilioInitializer(TwilioConfiguration twilioConfiguration) {
        this.twilioConfiguration = twilioConfiguration;
        Twilio.init(
                twilioConfiguration.getAccountSid(),
                twilioConfiguration.getAuthToken()
        );
        LOGGER.info(SUCCESS_MESSAGE);
    }
}