package com.telerikacademy.safetycar.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

import static com.telerikacademy.safetycar.config.ConfigConstants.Twilio.*;

@Configuration
@PropertySource("classpath:application.properties")
public class TwilioConfiguration {

    private String accountSid;
    private String authToken;
    private String trialNumber;

    @Autowired
    public TwilioConfiguration(Environment env) {
        accountSid = env.getProperty(SID);
        authToken = env.getProperty(TOKEN);
        trialNumber = env.getProperty(TRIAL_NUMBER);
    }

    public String getAccountSid() {
        return accountSid;
    }

    public String getAuthToken() {
        return authToken;
    }

    public String getTrialNumber() {
        return trialNumber;
    }

    public void setAccountSid(String accountSid) {
        this.accountSid = accountSid;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public void setTrialNumber(String trialNumber) {
        this.trialNumber = trialNumber;
    }
}