package com.telerikacademy.safetycar.services;

public interface ServicesConstants {

    final class Email {
        public static final String TITLE                                 = "title";
        public static final String MESSAGE                               = "message";
        public static final String EMAIL                                 = "email";
        public static final String LINK                                  = "link";
        public static final String LINK_VALUE                            = "http://localhost:8080/activation?token=";
        public static final String SEND_MAIL_HTML_TEMPLATE_URL           = "/emails/verification";
        public static final String FIRST_NAME                            = "firstName";
        public static final String LAST_NAME                             = "lastName";
        public static final String STATUS                                = "status";
        public static final String REQUEST_NUMBER                        = "requestNumber";
        public static final String CHANGE_STATUS_HTML_TEMPLATE_URL       = "/emails/status-change-email";
        public static final String HOLDER_FIRST_NAME                     = "holderFirstName";
        public static final String HOLDER_LAST_NAME                      = "holderLastName";
        public static final String OWNER_CHANGE_STATUS_HTML_TEMPLATE_URL = "/emails/status-change-mail-owner";
        public static final String CONTACT_MAIL_HTML_TEMPLATE_URL        = "/emails/contact-email";
        public static final String RESET_PASSWORD                        = "Reset password";
        public static final String SET_NEW_PASS_LINK                     = "http://localhost:8080/profile/set-new-password?token=";
        public static final String RESET_PASS_HTML_TEMPLATE_URL          = "/emails/reset-password-email";
        public static final String FORGOT_PASS_SUBJECT                   = "Forgot password change";
    }

    final class Messages {
        public static final String ADDRESS_VERIFICATION                    = "Email address verification";
        public static final String VERIFY_EMAIL_MESSAGE                    = "Verify your email address";
        public static final String POLICY_STATUS_CHANGED                   = "The status of your policy request has been changed!";
        public static final String POLICY_REQUEST_STATUS_CHANGED           = "Policy request status changed";
        public static final String POLICY_REQUEST_STATUS_CHANGED_EXCEPTION = "Policy request status changed";
        public static final String EMAIL_NOT_DELIVERED                     = "Email not delivered";
    }
}