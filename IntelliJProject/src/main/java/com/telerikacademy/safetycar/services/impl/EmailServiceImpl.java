package com.telerikacademy.safetycar.services.impl;

import com.telerikacademy.safetycar.exceptions.EmailNotSentException;
import com.telerikacademy.safetycar.models.PolicyRequest;
import com.telerikacademy.safetycar.models.User;
import com.telerikacademy.safetycar.models.VerificationToken;
import com.telerikacademy.safetycar.models.dtos.ContactDTO;
import com.telerikacademy.safetycar.repositories.contracts.VerificationTokenRepository;
import com.telerikacademy.safetycar.services.ServicesConstants.Email;
import com.telerikacademy.safetycar.services.ServicesConstants.Messages;
import com.telerikacademy.safetycar.services.contracts.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import static com.telerikacademy.safetycar.utils.Constants.SAFETY_CAR_EMAIL_ADDRESS;
import static com.telerikacademy.safetycar.utils.Constants.VERIFY_EMAIL_MESSAGE;

@Service
@PropertySource("classpath:application.properties")
public class EmailServiceImpl implements EmailService {
    private final VerificationTokenRepository verificationTokenRepository;
    private final SpringTemplateEngine        templateEngine;
    private final JavaMailSender              javaMailSender;

    @Autowired
    public EmailServiceImpl(VerificationTokenRepository verificationTokenRepository,
                            SpringTemplateEngine templateEngine,
                            JavaMailSender javaMailSender) {
        this.verificationTokenRepository = verificationTokenRepository;
        this.templateEngine = templateEngine;
        this.javaMailSender = javaMailSender;
    }

    @Override
    public void sendRegistrationMail(User user) {
        try {
            VerificationToken verificationToken = verificationTokenRepository.findByUser(user);
            if (verificationToken != null) {
                String token = verificationToken.getToken();
                Context context = new Context();
                context.setVariable(Email.TITLE, VERIFY_EMAIL_MESSAGE);
                context.setVariable(Email.LINK, Email.LINK_VALUE + token);
                String body = templateEngine.process(Email.SEND_MAIL_HTML_TEMPLATE_URL, context);
                MimeMessage message = javaMailSender.createMimeMessage();
                MimeMessageHelper helper = new MimeMessageHelper(message, true);
                helper.setTo(user.getUsername());
                helper.setSubject(Messages.ADDRESS_VERIFICATION);
                helper.setText(body, true);
                javaMailSender.send(message);
            }
        } catch (MessagingException e) {
            throw new EmailNotSentException(Messages.VERIFY_EMAIL_MESSAGE, user.getUsername());
        }
    }

    @Override
    public void sendStatusChangeEmail(PolicyRequest policyRequest, String email) {
        try {
            Context context = new Context();
            context.setVariable(Email.TITLE, Messages.POLICY_STATUS_CHANGED);
            context.setVariable(Email.FIRST_NAME, policyRequest.getFirstName());
            context.setVariable(Email.LAST_NAME, policyRequest.getLastName());
            context.setVariable(Email.STATUS, policyRequest.getStatus().getStatusName());
            context.setVariable(Email.REQUEST_NUMBER, policyRequest.getRequestNumber());
            String body = templateEngine.process(Email.CHANGE_STATUS_HTML_TEMPLATE_URL, context);
            MimeMessage message = javaMailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message, true);
            helper.setTo(email);
            helper.setSubject(Messages.POLICY_REQUEST_STATUS_CHANGED);
            helper.setText(body, true);
            javaMailSender.send(message);
            context.removeVariable(Email.FIRST_NAME);
            context.removeVariable(Email.LAST_NAME);

            if (!email.equals(policyRequest.getOwner().getEmail())) {
                context.setVariable(Email.FIRST_NAME, policyRequest.getOwner().getFirstName());
                context.setVariable(Email.LAST_NAME, policyRequest.getOwner().getLastName());
                context.setVariable(Email.HOLDER_FIRST_NAME, policyRequest.getFirstName());
                context.setVariable(Email.HOLDER_LAST_NAME, policyRequest.getLastName());
                String bodyOwner = templateEngine.process(Email.OWNER_CHANGE_STATUS_HTML_TEMPLATE_URL, context);
                MimeMessage messageOwner = javaMailSender.createMimeMessage();
                MimeMessageHelper helperOwner = new MimeMessageHelper(messageOwner, true);
                helperOwner.setTo(policyRequest.getOwner().getEmail());
                helperOwner.setSubject(Messages.POLICY_REQUEST_STATUS_CHANGED);
                helperOwner.setText(bodyOwner, true);
                javaMailSender.send(messageOwner);
            }
        } catch (MessagingException e) {
            throw new EmailNotSentException(Messages.POLICY_REQUEST_STATUS_CHANGED_EXCEPTION, email);
        }
    }

    @Override
    public void contactEmail(ContactDTO dto) {
        try {
            Context context = new Context();
            context.setVariable(Email.TITLE, dto.getName());
            context.setVariable(Email.MESSAGE, dto.getMessage());
            context.setVariable(Email.EMAIL, dto.getEmail());
            String body = templateEngine.process(Email.CONTACT_MAIL_HTML_TEMPLATE_URL, context);
            MimeMessage message = javaMailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message, true);
            helper.setTo(SAFETY_CAR_EMAIL_ADDRESS);
            helper.setSubject(dto.getSubject());
            helper.setText(body, true);
            javaMailSender.send(message);
        } catch (MessagingException e) {
            throw new EmailNotSentException(Messages.EMAIL_NOT_DELIVERED, SAFETY_CAR_EMAIL_ADDRESS);
        }
    }

    @Override
    public void sendResetPasswordEmail(User user, String token) {
        try {
            VerificationToken verificationToken = verificationTokenRepository.findByToken(token);
            if (verificationToken != null) {
                Context context = new Context();
                context.setVariable(Email.TITLE, Email.RESET_PASSWORD);
                context.setVariable(Email.LINK, Email.SET_NEW_PASS_LINK + token);
                String body = templateEngine.process(Email.RESET_PASS_HTML_TEMPLATE_URL, context);
                MimeMessage message = javaMailSender.createMimeMessage();
                MimeMessageHelper helper = new MimeMessageHelper(message, true);
                helper.setTo(user.getUsername());
                helper.setSubject(Email.FORGOT_PASS_SUBJECT);
                helper.setText(body, true);
                javaMailSender.send(message);
            }
        } catch (MessagingException e) {
            throw new EmailNotSentException(Messages.EMAIL_NOT_DELIVERED, user.getUsername());
        }
    }
}