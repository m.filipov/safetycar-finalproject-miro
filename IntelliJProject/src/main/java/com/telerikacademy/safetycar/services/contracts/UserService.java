package com.telerikacademy.safetycar.services.contracts;

import com.telerikacademy.safetycar.models.User;

import java.util.List;

public interface UserService {

    List<User> getAll();

    void update(User userToUpdate, User loggedUser);

    void delete(User userToDelete, User loggedUser);

    User getByEmail(String email);

    void resetPassword(User user);

    void changePass(User user, User loggedUser, String oldPass, String newPass, String confirmNewPass);

}