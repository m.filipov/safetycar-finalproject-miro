package com.telerikacademy.safetycar.services.contracts;

import com.telerikacademy.safetycar.models.Status;

import java.util.List;

public interface StatusService {

    List<Status> getAll();

    Status getById(int id);

    Status getByStatusName(String statusName);

}