package com.telerikacademy.safetycar.services.contracts;

import com.telerikacademy.safetycar.models.Question;
import com.telerikacademy.safetycar.models.Secret;
import com.telerikacademy.safetycar.models.User;
import com.telerikacademy.safetycar.models.UserInfo;

import java.util.List;

public interface SecretService {

    Secret findByAnswer(String answer);

    List<Secret> findByUserId(int userId);

    List<Secret> findByUser(User user);

    void save(UserInfo user, Question question, String answer);

}