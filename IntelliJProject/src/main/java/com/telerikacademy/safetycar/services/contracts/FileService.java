package com.telerikacademy.safetycar.services.contracts;

import org.springframework.web.multipart.MultipartFile;

public interface FileService {

    String uploadFile(MultipartFile file, int width);

    String getFileType(MultipartFile file);

}