package com.telerikacademy.safetycar.services.impl;

import com.telerikacademy.safetycar.models.SimulationOffer;
import com.telerikacademy.safetycar.repositories.contracts.SimulationOfferRepository;
import com.telerikacademy.safetycar.services.contracts.SimulationOfferService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.telerikacademy.safetycar.utils.Constants.*;

@Service
public class SimulationOfferServiceImpl implements SimulationOfferService {
    SimulationOfferRepository simulationOfferRepository;

    @Autowired
    public SimulationOfferServiceImpl(SimulationOfferRepository simulationOfferRepository) {
        this.simulationOfferRepository = simulationOfferRepository;
    }

    @Override
    public List<SimulationOffer> getAll() {
        return simulationOfferRepository.getAll();
    }

    @Override
    public SimulationOffer getById(int id) {
        return simulationOfferRepository.getById(id);
    }

    @Override
    public void create(SimulationOffer simulationOffer) {
        simulationOfferRepository.create(simulationOffer);
    }

    @Override
    public void update(SimulationOffer simulationOffer) {
        simulationOfferRepository.update(simulationOffer);
    }

    public double calculatePremium(SimulationOffer simulationOffer) {
        double netPremium;
        double totalPremium;
        double accidentCoeff = 1.00;
        double driverAgeCoeff = 1.00;
        double taxAmount;
        double baseAmount;
        if (simulationOffer.getPrevYearAccidents()) {
            accidentCoeff = ACCIDENT_COEFF;
        }
        if (simulationOffer.getDriverAge() < BASE_DRIVER_AGE) {
            driverAgeCoeff = DRIVER_AGE_COEFF;
        }
        baseAmount = simulationOfferRepository.getBaseAmountFromCsv(simulationOffer);
        netPremium = baseAmount * accidentCoeff * driverAgeCoeff;
        taxAmount = netPremium * TAX_AMOUNT;
        totalPremium = netPremium + taxAmount;
        return totalPremium;
    }
}