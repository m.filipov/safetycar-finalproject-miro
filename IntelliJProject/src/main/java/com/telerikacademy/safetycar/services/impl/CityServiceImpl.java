package com.telerikacademy.safetycar.services.impl;

import com.telerikacademy.safetycar.models.City;
import com.telerikacademy.safetycar.repositories.contracts.CityRepository;
import com.telerikacademy.safetycar.services.contracts.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CityServiceImpl implements CityService {
    private final CityRepository cityRepository;

    @Autowired
    public CityServiceImpl(CityRepository cityRepository) {
        this.cityRepository = cityRepository;
    }

    @Override
    public List<City> getAll() {
        return cityRepository.getAll();
    }

    @Override
    public City getById(int id) {
        return cityRepository.getById(id);
    }
}