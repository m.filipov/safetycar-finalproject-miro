package com.telerikacademy.safetycar.services.impl;

import com.telerikacademy.safetycar.models.Question;
import com.telerikacademy.safetycar.models.Secret;
import com.telerikacademy.safetycar.models.User;
import com.telerikacademy.safetycar.models.UserInfo;
import com.telerikacademy.safetycar.repositories.contracts.SecretRepository;
import com.telerikacademy.safetycar.services.contracts.SecretService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class SecretServiceImpl implements SecretService {
    private final SecretRepository secretRepository;

    @Autowired
    public SecretServiceImpl(SecretRepository secretRepository) {
        this.secretRepository = secretRepository;
    }

    @Override
    @Transactional
    public Secret findByAnswer(String answer) {
        return secretRepository.findByAnswer(answer);
    }

    @Override
    @Transactional
    public List<Secret> findByUserId(int userId) {
        return secretRepository.findByUserId(userId);
    }

    @Override
    @Transactional
    public List<Secret> findByUser(User user) {
        return secretRepository.findByUser(user);
    }

    @Override
    @Transactional
    public void save(UserInfo user, Question question, String answer) {
        Secret secret = new Secret(user, question, answer);
        secretRepository.save(secret);
    }
}