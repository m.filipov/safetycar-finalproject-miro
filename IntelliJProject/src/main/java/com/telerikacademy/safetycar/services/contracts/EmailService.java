package com.telerikacademy.safetycar.services.contracts;

import com.telerikacademy.safetycar.models.PolicyRequest;
import com.telerikacademy.safetycar.models.User;
import com.telerikacademy.safetycar.models.dtos.ContactDTO;

public interface EmailService {

    void sendRegistrationMail(User user);

    void sendStatusChangeEmail(PolicyRequest policyRequest, String email);

    void contactEmail(ContactDTO dto);

    void sendResetPasswordEmail(User user, String token);

}