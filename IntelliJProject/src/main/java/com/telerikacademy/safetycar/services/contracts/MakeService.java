package com.telerikacademy.safetycar.services.contracts;

import com.telerikacademy.safetycar.models.Make;

import java.util.List;

public interface MakeService {

    List<Make> getAll();

    Make getById(int id);

}