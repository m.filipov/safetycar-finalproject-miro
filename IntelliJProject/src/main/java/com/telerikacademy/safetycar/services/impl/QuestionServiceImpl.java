package com.telerikacademy.safetycar.services.impl;

import com.telerikacademy.safetycar.models.Question;
import com.telerikacademy.safetycar.repositories.contracts.QuestionRepository;
import com.telerikacademy.safetycar.services.contracts.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class QuestionServiceImpl implements QuestionService {
    private final QuestionRepository questionRepository;

    @Autowired
    public QuestionServiceImpl(QuestionRepository questionRepository) {
        this.questionRepository = questionRepository;
    }

    @Override
    public List<Question> findAll() {
        return questionRepository.findAll();
    }

    @Override
    public Question findById(int id) {
        return questionRepository.findById(id);
    }
}