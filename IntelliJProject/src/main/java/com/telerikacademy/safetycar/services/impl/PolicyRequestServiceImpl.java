package com.telerikacademy.safetycar.services.impl;

import com.telerikacademy.safetycar.exceptions.UnauthorizedOperationException;
import com.telerikacademy.safetycar.models.*;
import com.telerikacademy.safetycar.models.enums.PolicyStatus;
import com.telerikacademy.safetycar.repositories.contracts.PolicyRequestRepository;
import com.telerikacademy.safetycar.services.contracts.PolicyRequestService;
import com.telerikacademy.safetycar.services.contracts.StatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import static com.telerikacademy.safetycar.utils.Constants.NOT_AUTHORIZED_TO_CANCEL_POLICY_REQUEST_ERROR_MSG;
import static com.telerikacademy.safetycar.utils.Constants.NOT_AUTHORIZED_TO_APPROVE_DENY_POLICY_REQUEST_ERROR_MSG;
import static com.telerikacademy.safetycar.utils.Constants.REQUEST_NUMBER;

@Service
public class PolicyRequestServiceImpl implements PolicyRequestService {
    private final PolicyRequestRepository repository;
    private final StatusService statusService;

    @Autowired
    public PolicyRequestServiceImpl(PolicyRequestRepository repository, StatusService statusService) {
        this.repository = repository;
        this.statusService = statusService;
    }

    @Override
    public List<PolicyRequest> getAll() {
        return repository.getAll();
    }

    @Override
    public PolicyRequest getById(int id) {
        return repository.getById(id);
    }

    @Override
    public List<PolicyRequest> getByCreator(int userId) {
        return repository.getByCreator(userId);
    }

    @Override
    public void create(PolicyRequest policyRequest) {
        repository.create(policyRequest);
        policyRequest.setRequestNumber(policyRequest.getId() + REQUEST_NUMBER);
        repository.update(policyRequest);
    }

    @Override
    public List<PolicyRequest> filter(Integer status,
                                      String creatorEmail,
                                      String holderEmail,
                                      Integer requestNumber,
                                      String sortParam,
                                      String orderParam) {
        return repository.filter(Optional.ofNullable(status),
                Optional.ofNullable(creatorEmail),
                Optional.ofNullable(holderEmail),
                Optional.ofNullable(requestNumber),
                Optional.ofNullable(sortParam), orderParam);
    }

    @Override
    public void update(PolicyRequest policyRequest) {
        repository.update(policyRequest);
    }

    @Override
    public void approveOrDeny(PolicyRequest policyRequest, User loggedUser, String statusName) {
        if (loggedUser.isAdmin()
                && !policyRequest.getStatus().getStatusName().toLowerCase().equals(PolicyStatus.CANCELLED.getString())
                && !policyRequest.getEmail().equals(loggedUser.getUsername())
                && !userCreatedPolicy(policyRequest, loggedUser)) {
            Status status = statusService.getByStatusName(statusName.toLowerCase());
            policyRequest.setStatus(status);
            repository.update(policyRequest);
            return;
        }
        throw new UnauthorizedOperationException(NOT_AUTHORIZED_TO_APPROVE_DENY_POLICY_REQUEST_ERROR_MSG);
    }

    @Override
    public void cancel(PolicyRequest policyRequest, User loggedUser) {
        if (policyRequest.getStatus().getStatusName().toLowerCase().equals(PolicyStatus.PENDING.getString())
                && userCreatedPolicy(policyRequest, loggedUser)) {
            Status status = statusService.getByStatusName(PolicyStatus.CANCELLED.getString());
            policyRequest.setStatus(status);
            repository.update(policyRequest);
            return;
        }
        throw new UnauthorizedOperationException(NOT_AUTHORIZED_TO_CANCEL_POLICY_REQUEST_ERROR_MSG);
    }

    private boolean userCreatedPolicy(PolicyRequest policyRequest, User loggedUser) {
        return policyRequest.getOwner().getEmail().equals(loggedUser.getUsername());
    }
}