package com.telerikacademy.safetycar.services.contracts;

import com.telerikacademy.safetycar.models.Question;

import java.util.List;

public interface QuestionService {

    List<Question> findAll();

    Question findById(int id);

}