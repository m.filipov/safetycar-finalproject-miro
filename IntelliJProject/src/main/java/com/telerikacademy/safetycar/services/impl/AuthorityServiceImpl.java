package com.telerikacademy.safetycar.services.impl;

import com.telerikacademy.safetycar.models.Authority;
import com.telerikacademy.safetycar.repositories.contracts.AuthorityRepository;
import com.telerikacademy.safetycar.services.contracts.AuthorityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AuthorityServiceImpl implements AuthorityService {
    private final AuthorityRepository authorityRepository;

    @Autowired
    public AuthorityServiceImpl(AuthorityRepository authorityRepository) {
        this.authorityRepository = authorityRepository;
    }

    @Override
    public void create(Authority authority) {
        authorityRepository.create(authority);
    }

    @Override
    public void delete(Authority authority) {
        authorityRepository.delete(authority);
    }
}