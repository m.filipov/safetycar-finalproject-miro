package com.telerikacademy.safetycar.services.impl;

import com.telerikacademy.safetycar.exceptions.PasswordsDontMatchException;
import com.telerikacademy.safetycar.exceptions.SameOldAndNewPasswordException;
import com.telerikacademy.safetycar.exceptions.UnauthorizedOperationException;
import com.telerikacademy.safetycar.exceptions.WrongPassWordException;
import com.telerikacademy.safetycar.models.User;
import com.telerikacademy.safetycar.repositories.contracts.UserRepository;
import com.telerikacademy.safetycar.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.telerikacademy.safetycar.utils.Constants.*;

@Service
public class UserServiceImpl implements UserService {
    private final UserRepository  userRepository;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public List<User> getAll() {
        return userRepository.getAll();
    }

    @Override
    public void update(User userToUpdate, User loggedUser) {
        if (loggedUser.isAdmin() || loggedUser.getUsername().equals(userToUpdate.getUsername())) {
            userRepository.update(userToUpdate);
        } else {
            throw new UnauthorizedOperationException(NOT_ALLOWED_TO_EDIT_USERS_ERROR_MESSAGE);
        }
    }

    @Override
    public void delete(User userToDelete, User loggedUser) {
        checkIfLoggedUserIsAdmin(loggedUser);
        userRepository.delete(userToDelete);
    }

    @Override
    public User getByEmail(String email) {
        return userRepository.getByEmail(email);
    }

    @Override
    public void resetPassword(User user) {
        userRepository.update(user);
    }

    @Override
    public void changePass(User user, User loggedUser, String oldPass, String newPass, String confirmNewPass) {
        String encodedNewPass = passwordEncoder.encode(newPass);
        checkIfLoggedUserEqualsUser(user, loggedUser);
        checkOldPasswordMatches(user, oldPass);
        checkNewPassSameAsConfirmPass(newPass, confirmNewPass);
        checkIfOldPassEqualsNewPass(user, newPass);
        user.setPassword(encodedNewPass);
        userRepository.update(user);
    }

    private void checkIfOldPassEqualsNewPass(User user, String newPass) {
        if (passwordEncoder.matches(newPass, user.getPassword())) {
            throw new SameOldAndNewPasswordException(OLD_PASS_SAME_AS_NEW_ONE);
        }
    }

    private void checkNewPassSameAsConfirmPass(String newPass, String confirmNewPass) {
        if (!newPass.equals(confirmNewPass)) {
            throw new PasswordsDontMatchException(THE_PASSWORDS_DON_T_MATCH);
        }
    }

    private void checkOldPasswordMatches(User user, String oldPass) {
        if (!passwordEncoder.matches(oldPass, user.getPassword())) {
            throw new WrongPassWordException(WRONG_PASSWORD_ENTERED);
        }
    }

    private void checkIfLoggedUserEqualsUser(User user, User loggedUser) {
        if (!loggedUser.getUsername().equals(user.getUsername())) {
            throw new UnauthorizedOperationException(NOT_ALLOWED_TO_EDIT_USERS_ERROR_MESSAGE);
        }
    }

    private void checkIfLoggedUserIsAdmin(User loggedUser) {
        if (!loggedUser.isAdmin()) {
            throw new UnauthorizedOperationException(NOT_ALLOWED_TO_DELETE_USERS_ERROR_MESSAGE);
        }
    }
}