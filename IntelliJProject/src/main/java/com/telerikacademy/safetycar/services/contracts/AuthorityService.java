package com.telerikacademy.safetycar.services.contracts;

import com.telerikacademy.safetycar.models.Authority;

public interface AuthorityService {

    void create(Authority authority);

    void delete(Authority authority);

}