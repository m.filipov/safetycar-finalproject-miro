package com.telerikacademy.safetycar.services.impl;

import com.telerikacademy.safetycar.exceptions.FileStorageException;
import com.telerikacademy.safetycar.repositories.contracts.CloudStorageRepository;
import com.telerikacademy.safetycar.services.contracts.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Service
public class FileServiceImpl implements FileService {
    private final CloudStorageRepository cloudStorageRepository;

    @Autowired
    public FileServiceImpl(CloudStorageRepository cloudStorageRepository) {
        this.cloudStorageRepository = cloudStorageRepository;
    }

    @Override
    public String uploadFile(MultipartFile file, int width) {
        try {
            return cloudStorageRepository.uploadFile(file.getBytes(), getFileType(file), width);
        } catch (IOException | NullPointerException e) {
            throw new FileStorageException(e.getMessage());
        }
    }

    @Override
    public String getFileType(MultipartFile file) {
        String contentType = file.getContentType();
        return contentType.substring(0, contentType.indexOf("/"));
    }
}