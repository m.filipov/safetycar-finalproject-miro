package com.telerikacademy.safetycar.controllers.mvc;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import static com.telerikacademy.safetycar.controllers.mvc.MvcControllerConstants.HomeMvc.*;

@Controller
public class HomeMvcController {

    @GetMapping("/")
    public String getHomePage() {
        return HOME_INDEX;
    }

    @GetMapping(ACCESS_DENIED_ROUTE)
    public String showAccessDeniedPage() {
        return ACCESS_DENIED_HTML_ROUTE;
    }

    @GetMapping(TERMS)
    public String getTermsPage() {
        return TERMS_HTML_PATH;
    }

    @GetMapping(ABOUT_US)
    public String getAboutUsPage() {
        return ABOUT_US_HTML_PATH;
    }
}