package com.telerikacademy.safetycar.controllers.mvc;

import com.telerikacademy.safetycar.exceptions.InvalidInputException;
import com.telerikacademy.safetycar.models.PolicyRequest;
import com.telerikacademy.safetycar.models.SimulationOffer;
import com.telerikacademy.safetycar.models.UserInfo;
import com.telerikacademy.safetycar.models.dtos.CreatePolicyRequestDTO;
import com.telerikacademy.safetycar.services.contracts.CityService;
import com.telerikacademy.safetycar.services.contracts.PolicyRequestService;
import com.telerikacademy.safetycar.services.contracts.SimulationOfferService;
import com.telerikacademy.safetycar.services.contracts.UserInfoService;
import com.telerikacademy.safetycar.utils.PolicyRequestModelMapper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.security.Principal;

import static com.telerikacademy.safetycar.controllers.mvc.MvcControllerConstants.PolicyRequestMvc.*;

@Controller
@RequestMapping("/policy")
public class PolicyRequestMvcController {
    private final PolicyRequestService     policyRequestService;
    private final SimulationOfferService   simulationOfferService;
    private final CityService              cityService;
    private final UserInfoService          userInfoService;
    private final PolicyRequestModelMapper policyRequestModelMapper;

    public PolicyRequestMvcController(PolicyRequestService policyRequestService,
                                      SimulationOfferService simulationOfferService,
                                      CityService cityService,
                                      UserInfoService userInfoService,
                                      PolicyRequestModelMapper policyRequestModelMapper) {
        this.policyRequestService = policyRequestService;
        this.simulationOfferService = simulationOfferService;
        this.cityService = cityService;
        this.userInfoService = userInfoService;
        this.policyRequestModelMapper = policyRequestModelMapper;
    }

    @GetMapping
    public String showCreatePolicyRequestPage(Model model, HttpServletRequest request) {
        model.addAttribute(CREATE_POLICY_REQUEST_DTO, new CreatePolicyRequestDTO());

        HttpSession session = request.getSession(false);
        if (session.getAttribute(OFFER) == null) {
            return CREATE_POLICY_REQUEST_HTML_PATH;
        } else {
            SimulationOffer offer = (SimulationOffer) session.getAttribute(OFFER);
            model.addAttribute(OFFER, offer);
        }
        return CREATE_POLICY_REQUEST_HTML_PATH;
    }

    @PostMapping
    public String handleCreatePolicyRequest(@Valid @ModelAttribute(value = CREATE_POLICY_REQUEST_DTO) CreatePolicyRequestDTO createPolicyRequestDTO,
                                            BindingResult bindingResult,
                                            Principal principal,
                                            Model model,
                                            HttpSession session) {
        try {
            SimulationOffer offer = (SimulationOffer) session.getAttribute(OFFER);
            model.addAttribute(OFFER, offer);

            if (bindingResult.hasErrors()) {
                model.addAttribute(CREATE_POLICY_REQUEST_DTO, createPolicyRequestDTO);
                return CREATE_POLICY_REQUEST_HTML_PATH;
            }

            UserInfo loggedUserInfo = userInfoService.getByEmail(principal.getName());
            PolicyRequest policyRequest = policyRequestModelMapper.getPolicyRequestFromDTO(createPolicyRequestDTO, loggedUserInfo);
            simulationOfferService.create(offer);
            policyRequest.setSimulationOffer(offer);
            policyRequestService.create(policyRequest);
            return REDIRECT_POLICY_CONFIRMATION;
        } catch (InvalidInputException e) {
            model.addAttribute(INVALID_START_DATE, e.getMessage());
            return CREATE_POLICY_REQUEST_HTML_PATH;
        }
    }

    @GetMapping(POLICY_CONFIRMATION_ROUTE)
    public String showPolicyConfirmation(HttpSession session) {
        session.removeAttribute(OFFER);
        return POLICY_CONFIRMATION_HTML_PATH;
    }

    @ModelAttribute
    public void addAttributes(Model model) {
        model.addAttribute(CITIES, cityService.getAll());
    }
}