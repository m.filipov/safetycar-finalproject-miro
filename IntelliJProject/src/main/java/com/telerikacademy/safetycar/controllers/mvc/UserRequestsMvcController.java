package com.telerikacademy.safetycar.controllers.mvc;

import com.telerikacademy.safetycar.models.PolicyRequest;
import com.telerikacademy.safetycar.models.User;
import com.telerikacademy.safetycar.models.UserInfo;
import com.telerikacademy.safetycar.models.dtos.UpdatePolicyStatusDTO;
import com.telerikacademy.safetycar.services.contracts.PolicyRequestService;
import com.telerikacademy.safetycar.services.contracts.UserInfoService;
import com.telerikacademy.safetycar.services.contracts.UserService;
import com.telerikacademy.safetycar.utils.ConvertImage;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;

import static com.telerikacademy.safetycar.controllers.mvc.MvcControllerConstants.UserRequestsMvc.*;

@Controller
@RequestMapping("/my-requests")
public class UserRequestsMvcController {
    private final PolicyRequestService policyRequestService;
    private final UserService          userService;
    private final UserInfoService      userInfoService;

    public UserRequestsMvcController(PolicyRequestService policyRequestService,
                                     UserService userService,
                                     UserInfoService userInfoService) {
        this.policyRequestService = policyRequestService;
        this.userService = userService;
        this.userInfoService = userInfoService;
    }

    @GetMapping
    public String showAllRequests(Model model, Principal principal) {
        UserInfo userInfo = userInfoService.getByEmail(principal.getName());
        model.addAttribute(REQUESTS, policyRequestService.getByCreator(userInfo.getId()));
        model.addAttribute(DTO, new UpdatePolicyStatusDTO());
        model.addAttribute(CONVERT_IMAGE, new ConvertImage());
        return MY_REQUESTS_HTML_PATH;
    }

    @PostMapping(ID)
    public String cancelPolicyRequest(@PathVariable int id,
                                      Principal principal) {
        User loggedUser = userService.getByEmail(principal.getName());
        PolicyRequest policyRequest = policyRequestService.getById(id);
        policyRequestService.cancel(policyRequest, loggedUser);
        return REDIRECT_MY_REQUESTS;
    }
}