package com.telerikacademy.safetycar.controllers.mvc;

import com.telerikacademy.safetycar.exceptions.*;
import com.telerikacademy.safetycar.models.*;
import com.telerikacademy.safetycar.models.dtos.*;
import com.telerikacademy.safetycar.services.contracts.*;
import com.telerikacademy.safetycar.utils.UsersMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.lang.Nullable;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.security.Principal;
import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;

import static com.telerikacademy.safetycar.controllers.mvc.MvcControllerConstants.UserUpdateMvc.*;
import static com.telerikacademy.safetycar.utils.Constants.*;

@Controller
@RequestMapping("/profile")
public class UserUpdateMvcController {
    private final UserDetailsManager       userDetailsManager;
    private final UserService              userService;
    private final UserInfoService          userInfoService;
    private final UsersMapper              usersMapper;
    private final FileService              fileService;
    private final VerificationTokenService verificationTokenService;
    private final EmailService             emailService;
    private final QuestionService          questionService;
    private final SecretService            secretService;

    @Autowired
    public UserUpdateMvcController(UserDetailsManager userDetailsManager,
                                   UserService userService,
                                   UserInfoService userInfoService,
                                   UsersMapper usersMapper,
                                   FileService fileService,
                                   VerificationTokenService verificationTokenService,
                                   EmailService emailService, QuestionService questionService, SecretService secretService) {
        this.userDetailsManager = userDetailsManager;
        this.userService = userService;
        this.userInfoService = userInfoService;
        this.usersMapper = usersMapper;
        this.fileService = fileService;
        this.verificationTokenService = verificationTokenService;
        this.emailService = emailService;
        this.questionService = questionService;
        this.secretService = secretService;
    }

    @GetMapping
    public String showUserProfile(Model model, Principal principal) {
        try {
            UserInfo userInfo = userInfoService.getByEmail(principal.getName());
            UpdateUserDTO dto = usersMapper.showUserProfileDto(userInfo.getEmail());
            model.addAttribute(UPDATE_USER_DTO, dto);

            List<Secret> secrets = secretService.findByUserId(userInfo.getId());
            model.addAttribute(SECRETS, secrets);

            return USER_PROFILE_HTML_PATH;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping(UPDATE_ID_ROUTE)
    public String showEditProfile(@PathVariable int id, Model model) {
        try {
            UpdateUserDTO dto = usersMapper.toDto(id);
            model.addAttribute(UPDATE_USER_DTO, dto);
            return UPDATE_USER_DETAILS_HTML_PATH;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping(UPDATE_ID_ROUTE)
    public String updateUserDetails(@PathVariable int id,
                                    Model model,
                                    @Valid @ModelAttribute(value = UPDATE_USER_DTO) UpdateUserDTO dto,
                                    BindingResult bindingResult,
                                    Principal principal) {

        model.addAttribute(UPDATE_USER_DTO, dto);

        if (bindingResult.hasErrors()) {
            return UPDATE_USER_DETAILS_HTML_PATH;
        }

        UserInfo userInfo = userInfoService.getById(id);
        User loggedUser = userService.getByEmail(principal.getName());
        User user = userService.getByEmail(userInfo.getEmail());

        userInfo.setFirstName(dto.getFirstName());
        userInfo.setLastName(dto.getLastName());
        userInfo.setPhone(dto.getPhone());

        MultipartFile picture = dto.getPicture();
        checkIfPictureExists(userInfo, picture);
        user.setUserInfo(userInfo);
        userInfoService.update(userInfo);
        userService.update(user, loggedUser);
        return REDIRECT_PROFILE;
    }

    @GetMapping(CHANGE_PASSWORD_ROUTE)
    public String showChangePass(Model model, Principal principal) {
        UserInfo userInfo = userInfoService.getByEmail(principal.getName());
        ChangePassFromProfileDTO dto = new ChangePassFromProfileDTO();
        dto.setUserId(userInfo.getId());
        model.addAttribute(CHANGE_PASS, dto);
        return UPDATE_USER_PASS_HTML_PATH;
    }

    @PostMapping(CHANGE_PASS_ID_ROUTE)
    public String changePass(@PathVariable int id,
                             Model model,
                             @Valid @ModelAttribute(value = CHANGE_PASS) ChangePassFromProfileDTO dto,
                             BindingResult bindingResult,
                             Principal principal) {
        try {
            model.addAttribute(CHANGE_PASS, dto);
            if (bindingResult.hasErrors()) {
                return UPDATE_USER_PASS_HTML_PATH;
            }

            UserInfo userInfo = userInfoService.getById(id);
            User loggedUser = userService.getByEmail(principal.getName());
            User user = userService.getByEmail(userInfo.getEmail());
            checkIfChangePassPossible(dto, loggedUser, user);

        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (WrongPassWordException e) {
            model.addAttribute(WRONG_OR_OLD_PASS, e.getMessage());
            return UPDATE_USER_PASS_HTML_PATH;
        } catch (PasswordsDontMatchException e) {
            model.addAttribute(PASS_DONT_MATCH, e.getMessage());
            return UPDATE_USER_PASS_HTML_PATH;
        } catch (SameOldAndNewPasswordException e) {
            model.addAttribute(SAME_PASSWORDS, e.getMessage());
            return UPDATE_USER_PASS_HTML_PATH;
        }
        return REDIRECT_PROFILE;
    }

    @GetMapping(FORGOT_PASSWORD_ROUTE)
    public String showForgotPassword(Model model) {
        model.addAttribute(new ChangePasswordDTO());
        return USERS_FORGOT_PASSWORD_HTML_PATH;
    }

    @PostMapping(FORGOT_PASSWORD_ROUTE)
    public String emailForResetPasswordSent(@Valid @ModelAttribute(value = CHANGE_PASS_DTO) ChangePasswordDTO dto,
                                            Model model) {
        model.addAttribute(CHANGE_PASS_DTO, dto);

        if (!userDetailsManager.userExists(dto.getEmail())) {
            model.addAttribute(MAIL_NOT_FOUND, USER_WITH_THIS_EMAIL_NOT_FOUND);
            return USERS_FORGOT_PASSWORD_HTML_PATH;
        }

        try {
            User user = userService.getByEmail(dto.getEmail());
            String token = UUID.randomUUID().toString();
            verificationTokenService.save(user, token);
            emailService.sendResetPasswordEmail(user, token);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return RESET_PASSWORD_SENT_HTML_PATH;
    }

    @GetMapping(SET_NEW_PASSWORD_ROUTE)
    public String showResetPassword(@RequestParam(TOKEN) String token, Model model) {

        VerificationToken verificationToken = verificationTokenService.findByToken(token);
        Timestamp currentTimestamp = new Timestamp(System.currentTimeMillis());
        if (verificationToken.getExpiry().before(currentTimestamp)) {
            model.addAttribute(MESSAGE, VERIFICATION_TOKEN_EXPIRED_ERROR_MESSAGE);
        }
        ChangePasswordDTO dto = new ChangePasswordDTO();
        dto.setToken(verificationToken.getToken());
        model.addAttribute(CHANGE_PASSWORD_DTO, dto);
        return SET_NEW_PASSWORD_HTML_PATH;
    }

    @PostMapping(SET_NEW_PASSWORD_ROUTE)
    public String setNewPass(@Valid @ModelAttribute(value = CHANGE_PASSWORD_DTO) ChangePasswordDTO dto,
                             BindingResult bindingResult,
                             Model model) {

        VerificationToken verificationToken = verificationTokenService.findByToken(dto.getToken());
        User user = verificationToken.getUser();
        model.addAttribute(CHANGE_PASSWORD_DTO, dto);
        if (bindingResult.hasErrors()) {
            return REDIRECT_SET_NEW_PASSWORD + verificationToken.getToken();
        }

        String token = checkIfPasswordsMatch(dto, model, verificationToken);
        if (token != null) return token;
        user = usersMapper.fromDtoChangePass(user, dto);
        userService.resetPassword(user);
        return PASS_RESET_COMPLETED_HTML_PATH;
    }

    @GetMapping(RESET_PASSWORD_SENT_ROUTE)
    public String passwordMailSent() {
        return RESET_PASSWORD_SENT_HTML_PATH;
    }

    private void checkIfPictureExists(UserInfo userInfo, MultipartFile picture) {
        if (!picture.isEmpty()) {
            fileService.getFileType(picture);
            String pictureUrl = fileService.uploadFile(picture, 500);
            if (pictureUrl != null) {
                userInfo.setPicture(pictureUrl);
            }
        }
    }

    @GetMapping(ADD_SECRET_QUESTION_ROUTE)
    public String showAddSecretQuestion(Model model, Principal principal) {
        UserInfo userInfo = userInfoService.getByEmail(principal.getName());
        AddSecretDTO addSecretDTO = new AddSecretDTO();
        addSecretDTO.setUserId(userInfo.getId());
        model.addAttribute(ADD_SECRET_DTO, addSecretDTO);

        List<Question> questionsList = questionService.findAll();
        model.addAttribute(QUESTIONS, questionsList);

        List<Secret> secrets = secretService.findByUserId(userInfo.getId());
        model.addAttribute(QUESTIONS, secrets);

        return ADD_SECRET_QUESTION_HTML_PATH;
    }

    @GetMapping(EDIT_SECRET_QUESTIONS_ROUTE)
    public String showEditSecretQuestions(Principal principal, Model model) {
        try {
            UserInfo userInfo = userInfoService.getByEmail(principal.getName());
            int id = userInfo.getId();
            List<EditSecretDto> secrets = usersMapper.toSecretDto(id);
            model.addAttribute(SECRET, secrets.get(0));
            return EDIT_SECRET_QUESTIONS_HTML_PATH;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping(ADD_SECRET_QUESTIONS_ID_ROUTE)
    public String addSecretQuestion(@PathVariable int id,
                                    Model model,
                                    @Valid @ModelAttribute(value = ADD_SECRET_DTO) AddSecretDTO dto,
                                    BindingResult bindingResult,
                                    Principal principal) {
        if (bindingResult.hasErrors()) {
            return ADD_SECRET_QUESTION_HTML_PATH;
        }
        model.addAttribute(ADD_SECRET_DTO, dto);
        UserInfo userInfo = userInfoService.getByEmail(principal.getName());
        secretService.save(userInfo, questionService.findById(dto.getQuestionId()), dto.getAnswer());
        return REDIRECT_PROFILE;
    }

    private void checkIfChangePassPossible(ChangePassFromProfileDTO dto, User loggedUser, User user) {
        if (dto.getOldPass() != null && dto.getNewPass() != null && dto.getConfirmNewPass() != null) {
            userService.changePass(user, loggedUser, dto.getOldPass(), dto.getNewPass(), dto.getConfirmNewPass());
        }
    }

    @Nullable
    private String checkIfPasswordsMatch(ChangePasswordDTO dto, Model model, VerificationToken verificationToken) {
        if (!dto.getPassword().equals(dto.getConfirmPassword())) {
            model.addAttribute(CHANGE_PASS_DTO, dto);
            model.addAttribute(PASS_MATCH, THE_PASSWORDS_DON_T_MATCH);
            return REDIRECT_SET_NEW_PASSWORD + verificationToken.getToken();
        }
        return null;
    }
}