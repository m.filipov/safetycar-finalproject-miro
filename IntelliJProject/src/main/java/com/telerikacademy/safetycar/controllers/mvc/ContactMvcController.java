package com.telerikacademy.safetycar.controllers.mvc;

import com.telerikacademy.safetycar.models.dtos.ContactDTO;
import com.telerikacademy.safetycar.services.contracts.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

import static com.telerikacademy.safetycar.controllers.mvc.MvcControllerConstants.ContactMvc.*;

@Controller
@RequestMapping(CONTACTS_ROUTE)
public class ContactMvcController {
    private final EmailService emailService;

    @Autowired
    public ContactMvcController(EmailService emailService) {
        this.emailService = emailService;
    }

    @GetMapping
    public String showContactForm(Model model) {
        model.addAttribute(CONTACT, new ContactDTO());
        return CONTACTS_HTML_ROUTE;
    }

    @PostMapping
    public String contact(@Valid @ModelAttribute(name = CONTACT) ContactDTO dto,
                          BindingResult bindingResult,
                          Model model) {
        model.addAttribute(CONTACT, dto);

        if (bindingResult.hasErrors()) {
            return CONTACTS_HTML_ROUTE;
        }
        emailService.contactEmail(dto);
        return CONFIRMATION_MESSAGE_SENT;
    }
}