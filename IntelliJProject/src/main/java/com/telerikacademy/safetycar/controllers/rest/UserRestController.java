package com.telerikacademy.safetycar.controllers.rest;

import com.telerikacademy.safetycar.exceptions.DuplicateEntityException;
import com.telerikacademy.safetycar.exceptions.EntityNotFoundException;
import com.telerikacademy.safetycar.exceptions.UnauthorizedOperationException;
import com.telerikacademy.safetycar.models.User;
import com.telerikacademy.safetycar.models.UserInfo;
import com.telerikacademy.safetycar.models.dtos.ShowUsersDTO;
import com.telerikacademy.safetycar.services.contracts.UserInfoService;
import com.telerikacademy.safetycar.services.contracts.UserService;
import com.telerikacademy.safetycar.utils.UsersMapper;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

import static com.telerikacademy.safetycar.controllers.rest.RestControllerConstants.ID;
import static com.telerikacademy.safetycar.controllers.rest.RestControllerConstants.USERS_ROUTE;
import static com.telerikacademy.safetycar.utils.Constants.*;
import static com.telerikacademy.safetycar.utils.LoggedUserValidator.checkIfUserIsLogged;

@RestController
@RequestMapping(USERS_ROUTE)
public class UserRestController {
    private final UserService userService;
    private final UserInfoService userInfoService;
    private final UsersMapper usersMapper;

    @Autowired
    public UserRestController(UserService userService,
                              UserInfoService userInfoService,
                              UsersMapper usersMapper) {
        this.userService = userService;
        this.userInfoService = userInfoService;
        this.usersMapper = usersMapper;
    }

    @GetMapping
    @ApiOperation(value = SHOW_ALL_USERS, response = List.class)
    public List<ShowUsersDTO> getAll() {
        return usersMapper.getShowAllUsers(userInfoService.getAll());
    }

    @GetMapping(ID)
    @ApiOperation(value = GET_USER_BY_ID, response = ShowUsersDTO.class)
    public ShowUsersDTO getById(@PathVariable int id, Principal principal) {
        try {
            checkIfUserIsLogged(principal);
            UserInfo userInfo = userInfoService.getById(id);
            return usersMapper.getShowUsersDto(userInfo);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PutMapping
    @ApiOperation(value = UPDATE_USER, response = UserInfo.class)
    public UserInfo update(@Valid @RequestBody UserInfo user, Principal principal) {
        try {
            checkIfUserIsLogged(principal);
            User loggedUser = userService.getByEmail(principal.getName());
            userInfoService.modify(user, loggedUser);
            return user;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, USER_WITH_THIS_ID_EXISTS_ERROR_MESSAGE);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException ex) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, ex.getMessage());
        }
    }
}