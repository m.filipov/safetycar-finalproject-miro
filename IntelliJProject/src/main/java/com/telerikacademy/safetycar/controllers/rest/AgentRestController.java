package com.telerikacademy.safetycar.controllers.rest;

import com.telerikacademy.safetycar.exceptions.EntityNotFoundException;
import com.telerikacademy.safetycar.exceptions.UnauthorizedOperationException;
import com.telerikacademy.safetycar.models.PolicyRequest;
import com.telerikacademy.safetycar.models.Status;
import com.telerikacademy.safetycar.models.User;
import com.telerikacademy.safetycar.models.dtos.ShowRequestsDTO;
import com.telerikacademy.safetycar.models.dtos.UpdatePolicyStatusRestDTO;
import com.telerikacademy.safetycar.services.contracts.EmailService;
import com.telerikacademy.safetycar.services.contracts.PolicyRequestService;
import com.telerikacademy.safetycar.services.contracts.UserService;
import com.telerikacademy.safetycar.utils.PolicyRequestModelMapper;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

import static com.telerikacademy.safetycar.controllers.rest.RestControllerConstants.*;
import static com.telerikacademy.safetycar.utils.Constants.*;
import static com.telerikacademy.safetycar.utils.LoggedUserValidator.checkIfUserIsLogged;

@RestController
@RequestMapping(AGENT_REQUESTS_ROUTE)
public class AgentRestController {
    private final PolicyRequestService     policyRequestService;
    private final UserService              userService;
    private final PolicyRequestModelMapper policyRequestModelMapper;
    private final EmailService             emailService;

    @Autowired
    public AgentRestController(PolicyRequestService policyRequestService,
                               UserService userService,
                               PolicyRequestModelMapper policyRequestModelMapper,
                               EmailService emailService) {
        this.policyRequestService = policyRequestService;
        this.userService = userService;
        this.policyRequestModelMapper = policyRequestModelMapper;
        this.emailService = emailService;
    }

    @GetMapping
    @ApiOperation(value = SHOW_ALL_REQUESTS_IN_THE_SYSTEM, response = List.class)
    public List<ShowRequestsDTO> showRequests(Principal principal) {
        checkIfUserIsLogged(principal);
        return policyRequestModelMapper.getShowAll(policyRequestService.getAll());
    }

    @GetMapping(ID)
    @ApiOperation(value = GET_POLICY_REQUEST_BY_ID, response = ShowRequestsDTO.class)
    public ShowRequestsDTO getById(@PathVariable int id, Principal principal) {
        try {
            checkIfUserIsLogged(principal);
            PolicyRequest policyRequest = policyRequestService.getById(id);
            return policyRequestModelMapper.getShowRequestsDto(policyRequest);

        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping(FILTER_ROUTE)
    @ApiOperation(value = FILTER_POLICY_REQUESTS, response = List.class)
    public List<PolicyRequest> filter(@RequestParam(required = false) Integer statusId,
                                      @RequestParam(required = false) String creatorEmail,
                                      @RequestParam(required = false) String holderEmail,
                                      @RequestParam(required = false) Integer requestNumber,
                                      @RequestParam(required = false) String sortParam,
                                      @RequestParam() String orderParam,
                                      Principal principal) {
        checkIfUserIsLogged(principal);
        return policyRequestService.filter(statusId, creatorEmail, holderEmail, requestNumber, sortParam, orderParam);
    }

    @PutMapping
    @ApiOperation(value = APPROVE_OR_DENY_POLICY_REQUEST, response = ShowRequestsDTO.class)
    public ShowRequestsDTO approveOrDeny(@Valid @RequestBody UpdatePolicyStatusRestDTO dto, Principal principal) {
        try {
            checkIfUserIsLogged(principal);
            User loggedUser = userService.getByEmail(principal.getName());
            PolicyRequest policyRequest = policyRequestService.getById(dto.getId());
            Status status = new Status();
            status.setStatusName(dto.getStatus());
            policyRequestService.approveOrDeny(policyRequest, loggedUser, status.getStatusName());
            String email = policyRequest.getEmail();
            emailService.sendStatusChangeEmail(policyRequest, email);
            return policyRequestModelMapper.getShowRequestsDto(policyRequest);

        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }
}