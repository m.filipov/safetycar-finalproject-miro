package com.telerikacademy.safetycar.controllers.mvc;

import com.telerikacademy.safetycar.models.PolicyRequest;
import com.telerikacademy.safetycar.models.User;
import com.telerikacademy.safetycar.models.dtos.FilterRequestsDTO;
import com.telerikacademy.safetycar.models.dtos.UpdatePolicyStatusDTO;
import com.telerikacademy.safetycar.services.contracts.EmailService;
import com.telerikacademy.safetycar.services.contracts.PolicyRequestService;
import com.telerikacademy.safetycar.services.contracts.StatusService;
import com.telerikacademy.safetycar.services.contracts.UserService;
import com.telerikacademy.safetycar.utils.ConvertImage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

import static com.telerikacademy.safetycar.controllers.mvc.MvcControllerConstants.AgentMvc.*;

@Controller
@RequestMapping(REQUESTS_ROUTE)
public class AgentMvcController {
    private final PolicyRequestService policyRequestService;
    private final UserService          userService;
    private final EmailService         emailService;
    private final StatusService        statusService;

    @Autowired
    public AgentMvcController(PolicyRequestService policyRequestService,
                              UserService userService,
                              EmailService emailService,
                              StatusService statusService) {
        this.policyRequestService = policyRequestService;
        this.userService = userService;
        this.emailService = emailService;
        this.statusService = statusService;
    }

    @GetMapping
    public String showAllRequests(Model model) {
        model.addAttribute(DTO, new UpdatePolicyStatusDTO());
        model.addAttribute(FILTER_REQUESTS_DTO, new FilterRequestsDTO());
        model.addAttribute(CONVERT_IMAGE, new ConvertImage());
        model.addAttribute(STATUSES, statusService.getAll());
        return AGENT_REQUESTS_ROUTE;
    }

    @PostMapping(ID)
    public String changeStatusPolicyRequest(@PathVariable int id,
                                            @ModelAttribute(value = DTO) UpdatePolicyStatusDTO dto,
                                            Principal principal,
                                            Model model) {
        model.addAttribute(DTO, dto);
        User loggedUser = userService.getByEmail(principal.getName());
        PolicyRequest policyRequest = policyRequestService.getById(id);
        String status = dto.getStatus();
        policyRequestService.approveOrDeny(policyRequest, loggedUser, status);
        String email = policyRequest.getEmail();
        emailService.sendStatusChangeEmail(policyRequest, email);
        return REDIRECT_REQUESTS;
    }

    @PostMapping()
    public String filterRequests(FilterRequestsDTO dto, Model model) {
        model.addAttribute(DTO, new UpdatePolicyStatusDTO());
        model.addAttribute(STATUSES, statusService.getAll());
        model.addAttribute(FILTER_REQUESTS_DTO, dto);
        model.addAttribute(CONVERT_IMAGE, new ConvertImage());
        List<PolicyRequest> filter = policyRequestService.filter(dto.getStatus(),
                dto.getCreatorEmail(),
                dto.getHolderEmail(),
                dto.getRequestNumber(),
                dto.sortParam,
                dto.orderParam);
        model.addAttribute(REQUESTS, filter);
        return AGENT_REQUESTS_ROUTE;
    }

    @ModelAttribute
    public void addAttributes(Model model) {
        model.addAttribute(REQUESTS, policyRequestService.getAll());
    }
}