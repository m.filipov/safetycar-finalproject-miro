package com.telerikacademy.safetycar.controllers.rest;

import com.telerikacademy.safetycar.exceptions.EntityNotFoundException;
import com.telerikacademy.safetycar.models.City;
import com.telerikacademy.safetycar.services.contracts.CityService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

import static com.telerikacademy.safetycar.controllers.rest.RestControllerConstants.CITIES_ROUTE;
import static com.telerikacademy.safetycar.controllers.rest.RestControllerConstants.ID;
import static com.telerikacademy.safetycar.utils.Constants.GET_CITY_BY_ID;
import static com.telerikacademy.safetycar.utils.Constants.LIST_ALL_CITIES;

@RestController
@RequestMapping(CITIES_ROUTE)
public class CityRestController {
    private final CityService cityService;

    @Autowired
    public CityRestController(CityService cityService) {
        this.cityService = cityService;
    }

    @GetMapping
    @ApiOperation(value = LIST_ALL_CITIES, response = List.class)
    public List<City> getAll() {
        return cityService.getAll();
    }

    @GetMapping(ID)
    @ApiOperation(value = GET_CITY_BY_ID, response = City.class)
    public City getById(@PathVariable int id) {
        try {
            return cityService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}