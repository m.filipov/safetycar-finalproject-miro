package com.telerikacademy.safetycar.controllers.mvc;

public interface MvcControllerConstants {

    final class AdminMvc {
        public static final String ADMINS_ROUTE       = "/admins";
        public static final String ADMINS_HTML_PATH   = "admins/admins";
        public static final String SHOW_USERS_DTO     = "showUsersDto";
        public static final String USERS              = "users";
        public static final String ADD_BY_ID_ROUTE    = "/add/{id}";
        public static final String REMOVE_BY_ID_ROUTE = "/remove/{id}";
        public static final String REDIRECT_ADMINS    = "redirect:/admins";
    }

    final class AgentMvc {
        public static final String REQUESTS_ROUTE       = "/requests";
        public static final String DTO                  = "dto";
        public static final String FILTER_REQUESTS_DTO  = "filterRequestsDto";
        public static final String CONVERT_IMAGE        = "convertImage";
        public static final String STATUSES             = "statuses";
        public static final String AGENT_REQUESTS_ROUTE = "requests/requests-agents";
        public static final String ID                   = "/{id}";
        public static final String REDIRECT_REQUESTS    = "redirect:/requests";
        public static final String REQUESTS             = "requests";
    }

    final class ContactMvc {
        public static final String CONTACTS_ROUTE            = "/contact";
        public static final String CONTACT                   = "contact";
        public static final String CONTACTS_HTML_ROUTE       = "contact/contact";
        public static final String CONFIRMATION_MESSAGE_SENT = "confirmations/contact-message-sent";
    }

    final class HomeMvc {
        public static final String HOME_INDEX               = "home/index";
        public static final String ACCESS_DENIED_ROUTE      = "/access-denied";
        public static final String ACCESS_DENIED_HTML_ROUTE = "errors/access-denied";
        public static final String TERMS                    = "/terms";
        public static final String TERMS_HTML_PATH          = "home/terms";
        public static final String ABOUT_US                 = "/about-us";
        public static final String ABOUT_US_HTML_PATH       = "home/about-us";
    }

    final class LoginMvc {
        public static final String LOGIN_ROUTE     = "/login";
        public static final String LOGIN_HTML_PATH = "users/login";
    }

    final class PolicyRequestMvc {
        public static final String CREATE_POLICY_REQUEST_DTO       = "createPolicyRequestDTO";
        public static final String CREATE_POLICY_REQUEST_HTML_PATH = "requests/create-policy-request";
        public static final String OFFER                           = "offer";
        public static final String REDIRECT_POLICY_CONFIRMATION    = "redirect:/policy/policy-confirmation";
        public static final String INVALID_START_DATE              = "invalidStartDate";
        public static final String POLICY_CONFIRMATION_ROUTE       = "/policy-confirmation";
        public static final String POLICY_CONFIRMATION_HTML_PATH   = "confirmations/policy-confirmation";
        public static final String CITIES                          = "cities";
    }

    final class RegistrationMvc {
        public static final String REGISTRATION_CONFIRMATION_ROUTE     = "/registration-confirmation";
        public static final String REGISTRATION_CONFIRMATION_HTML_PATH = "confirmations/registration-confirmation";
        public static final String REGISTER_ROUTE                      = "/register";
        public static final String USER                                = "user";
        public static final String REGISTER_HTML_PATH                  = "users/register";
        public static final String RECAPTCHA_RESPONSE                  = "g-recaptcha-response";
        public static final String PASS_MATCH                          = "passMatch";
        public static final String MAIL_TAKEN                          = "mailTaken";
        public static final String RECAPTCHA_ERROR                     = "reCaptchaError";
        public static final String REDIRECT_REGISTRATION_CONFIRMATION  = "redirect:/registration-confirmation";
        public static final String ACTIVATION_ROUTE                    = "/activation";
        public static final String TOKEN                               = "token";
        public static final String MESSAGE                             = "message";
        public static final String CONFIRMATION_ACTIVATION_HTML_PATH   = "confirmations/activation";
    }

    final class SimulationOfferMvc {
        public static final String OFFER_ROUTE                       = "/offer";
        public static final String SIMULATION_OFFER_DTO              = "simulationOfferDTO";
        public static final String CREATE_SIMULATION_OFFER_HTML_PATH = "offer/create-simulation-offer";
        public static final String OFFER                             = "offer";
        public static final String REGISTRATION_DATE_ERROR           = "regDateError";
        public static final String MAKES                             = "makes";
        public static final String MODELS                            = "models";
    }

    final class UserRequestsMvc {
        public static final String REQUESTS              = "requests";
        public static final String DTO                   = "dto";
        public static final String CONVERT_IMAGE         = "convertImage";
        public static final String MY_REQUESTS_HTML_PATH = "requests/my-requests";
        public static final String REDIRECT_MY_REQUESTS  = "redirect:/my-requests";
        public static final String ID                    = "/{id}";
    }

    final class UserUpdateMvc {
        public static final String UPDATE_USER_DTO                 = "updateUserDto";
        public static final String SECRETS                         = "secrets";
        public static final String USER_PROFILE_HTML_PATH          = "users/user-profile";
        public static final String UPDATE_ID_ROUTE                 = "/update/{id}";
        public static final String UPDATE_USER_DETAILS_HTML_PATH   = "users/update-user-details";
        public static final String REDIRECT_PROFILE                = "redirect:/profile";
        public static final String CHANGE_PASSWORD_ROUTE           = "/change-pass";
        public static final String CHANGE_PASS                     = "changePass";
        public static final String UPDATE_USER_PASS_HTML_PATH      = "users/update-user-pass";
        public static final String CHANGE_PASS_ID_ROUTE            = "/change-pass/{id}";
        public static final String WRONG_OR_OLD_PASS               = "wrongOldPass";
        public static final String PASS_DONT_MATCH                 = "passDontMatch";
        public static final String SAME_PASSWORDS                  = "samePasswords";
        public static final String FORGOT_PASSWORD_ROUTE           = "/forgot-password";
        public static final String USERS_FORGOT_PASSWORD_HTML_PATH = "users/forgot-password";
        public static final String CHANGE_PASS_DTO                 = "changePasswordDto";
        public static final String MAIL_NOT_FOUND                  = "mailNotFound";
        public static final String RESET_PASSWORD_SENT_HTML_PATH   = "confirmations/reset-password-sent";
        public static final String SET_NEW_PASSWORD_ROUTE          = "/set-new-password";
        public static final String TOKEN                           = "token";
        public static final String MESSAGE                         = "message";
        public static final String CHANGE_PASSWORD_DTO             = "changePasswordDto";
        public static final String SET_NEW_PASSWORD_HTML_PATH      = "users/set-new-password";
        public static final String REDIRECT_SET_NEW_PASSWORD       = "redirect:/profile/set-new-password?token=";
        public static final String PASS_RESET_COMPLETED_HTML_PATH  = "confirmations/password-reset-completed";
        public static final String RESET_PASSWORD_SENT_ROUTE       = "/reset-password-sent";
        public static final String ADD_SECRET_QUESTION_ROUTE       = "/add-secret-question";
        public static final String ADD_SECRET_DTO                  = "addSecretDto";
        public static final String QUESTIONS                       = "questions";
        public static final String ADD_SECRET_QUESTION_HTML_PATH   = "users/add-secret-question";
        public static final String EDIT_SECRET_QUESTIONS_ROUTE     = "/edit-secret-questions";
        public static final String SECRET                          = "secret";
        public static final String EDIT_SECRET_QUESTIONS_HTML_PATH = "users/edit-secret-questions";
        public static final String ADD_SECRET_QUESTIONS_ID_ROUTE   = "/add-secret-question/{id}";
        public static final String PASS_MATCH                      = "passMatch";
    }
}
