package com.telerikacademy.safetycar.controllers.mvc;

import com.telerikacademy.safetycar.exceptions.EntityNotFoundException;
import com.telerikacademy.safetycar.exceptions.UnauthorizedOperationException;
import com.telerikacademy.safetycar.models.Authority;
import com.telerikacademy.safetycar.models.User;
import com.telerikacademy.safetycar.models.UserInfo;
import com.telerikacademy.safetycar.models.dtos.ShowUsersDTO;
import com.telerikacademy.safetycar.services.contracts.AuthorityService;
import com.telerikacademy.safetycar.services.contracts.UserInfoService;
import com.telerikacademy.safetycar.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;
import java.util.Optional;

import static com.telerikacademy.safetycar.controllers.mvc.MvcControllerConstants.AdminMvc.*;
import static com.telerikacademy.safetycar.utils.Constants.*;

@Controller
@RequestMapping(ADMINS_ROUTE)
public class AdminMvcController {
    private final UserService      userService;
    private final UserInfoService  userInfoService;
    private final AuthorityService authorityService;

    @Autowired
    public AdminMvcController(UserService userService,
                              UserInfoService userInfoService,
                              AuthorityService authorityService) {
        this.userService = userService;
        this.userInfoService = userInfoService;
        this.authorityService = authorityService;
    }

    @GetMapping
    public String showAllUsers(Model model) {
        model.addAttribute(SHOW_USERS_DTO, new ShowUsersDTO());
        model.addAttribute(USERS, userService.getAll());
        return ADMINS_HTML_PATH;
    }

    @PostMapping(ADD_BY_ID_ROUTE)
    public String addAgentAccess(@PathVariable int id, Principal principal) {
        UserInfo userInfo = userInfoService.getById(id);
        User loggedUser = userService.getByEmail(principal.getName());
        Authority authority = new Authority();
        authority.setUsername(userInfo.getEmail());
        authority.setAuthority(ROLE_ADMIN);
        ensureAuthorized(loggedUser);
        authorityService.create(authority);
        return REDIRECT_ADMINS;
    }

    @PostMapping(REMOVE_BY_ID_ROUTE)
    public String removeAgent(@PathVariable int id, Principal principal) {
        UserInfo userInfo = userInfoService.getById(id);
        User loggedUser = userService.getByEmail(principal.getName());
        User userToRemove = userService.getByEmail(userInfo.getEmail());
        Optional<Authority> authority = userToRemove.getAuthorities().stream().filter(a -> a.getAuthority().equals(ROLE_ADMIN)).findAny();
        ensureAuthorized(loggedUser);
        Authority authorityToDelete = authority.orElseThrow(() -> new EntityNotFoundException(USER_NOT_AN_ADMIN));
        authorityService.delete(authorityToDelete);
        return REDIRECT_ADMINS;
    }

    private void ensureAuthorized(User loggedUser) {
        if (!loggedUser.isFullAdmin()) {
            throw new UnauthorizedOperationException(AGENT_ACCESS_ERROR);
        }
    }
}