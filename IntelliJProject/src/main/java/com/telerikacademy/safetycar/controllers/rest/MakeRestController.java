package com.telerikacademy.safetycar.controllers.rest;

import com.telerikacademy.safetycar.exceptions.EntityNotFoundException;
import com.telerikacademy.safetycar.models.Make;
import com.telerikacademy.safetycar.services.contracts.MakeService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

import static com.telerikacademy.safetycar.controllers.rest.RestControllerConstants.ID;
import static com.telerikacademy.safetycar.controllers.rest.RestControllerConstants.MAKES_ROUTE;
import static com.telerikacademy.safetycar.utils.Constants.GET_ALL_CAR_MAKES;
import static com.telerikacademy.safetycar.utils.Constants.LIST_ALL_CAR_MAKES;

@RestController
@RequestMapping(MAKES_ROUTE)
public class MakeRestController {
    private final MakeService makeService;

    @Autowired
    public MakeRestController(MakeService makeService) {
        this.makeService = makeService;
    }

    @GetMapping
    @ApiOperation(value = LIST_ALL_CAR_MAKES, response = List.class)
    public List<Make> getAll() {
        return makeService.getAll();
    }

    @GetMapping(ID)
    @ApiOperation(value = GET_ALL_CAR_MAKES, response = Make.class)
    public Make getById(@PathVariable int id) {
        try {
            return makeService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}