package com.telerikacademy.safetycar.controllers.rest;

import com.telerikacademy.safetycar.exceptions.EntityNotFoundException;
import com.telerikacademy.safetycar.models.PolicyRequest;
import com.telerikacademy.safetycar.models.User;
import com.telerikacademy.safetycar.models.UserInfo;
import com.telerikacademy.safetycar.services.contracts.PolicyRequestService;
import com.telerikacademy.safetycar.services.contracts.UserInfoService;
import com.telerikacademy.safetycar.services.contracts.UserService;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.security.Principal;
import java.util.List;

import static com.telerikacademy.safetycar.controllers.rest.RestControllerConstants.MY_REQUESTS_ROUTE;
import static com.telerikacademy.safetycar.utils.Constants.CANCEL_POLICY_REQUEST;
import static com.telerikacademy.safetycar.utils.Constants.SHOW_ALL_REQUESTS_OF_LOGGED_USER;
import static com.telerikacademy.safetycar.utils.LoggedUserValidator.checkIfUserIsLogged;

@RestController
@RequestMapping(MY_REQUESTS_ROUTE)
public class UserRequestsRestController {
    private final PolicyRequestService policyRequestService;
    private final UserService          userService;
    private final UserInfoService      userInfoService;

    public UserRequestsRestController(PolicyRequestService policyRequestService,
                                      UserService userService,
                                      UserInfoService userInfoService) {
        this.policyRequestService = policyRequestService;
        this.userService = userService;
        this.userInfoService = userInfoService;
    }

    @GetMapping
    @ApiOperation(value = SHOW_ALL_REQUESTS_OF_LOGGED_USER, response = List.class)
    public List<PolicyRequest> showAllRequests(Principal principal) {
        try {
            checkIfUserIsLogged(principal);
            UserInfo loggedUserInfo = userInfoService.getByEmail(principal.getName());
            return policyRequestService.getByCreator(loggedUserInfo.getId());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    @ApiOperation(value = CANCEL_POLICY_REQUEST, response = PolicyRequest.class)
    public PolicyRequest cancelPolicyRequest(@RequestBody int id,
                                             Principal principal) {
        try {
            checkIfUserIsLogged(principal);
            User loggedUser = userService.getByEmail(principal.getName());
            PolicyRequest policyRequest = policyRequestService.getById(id);
            policyRequestService.cancel(policyRequest, loggedUser);
            return policyRequest;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}