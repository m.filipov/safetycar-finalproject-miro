package com.telerikacademy.safetycar.controllers.rest;

import com.telerikacademy.safetycar.exceptions.InvalidInputException;
import com.telerikacademy.safetycar.models.SimulationOffer;
import com.telerikacademy.safetycar.models.dtos.SimulationOfferDTO;
import com.telerikacademy.safetycar.services.contracts.SimulationOfferService;
import com.telerikacademy.safetycar.utils.SimulationOfferMapper;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

import static com.telerikacademy.safetycar.controllers.rest.RestControllerConstants.OFFER_ROUTE;
import static com.telerikacademy.safetycar.controllers.rest.RestControllerConstants.TOTAL_PREMIUM;
import static com.telerikacademy.safetycar.utils.Constants.CALCULATE_AND_SHOW_PREMIUM_FOR_SIMULATION_OFFER;

@RestController
@RequestMapping(OFFER_ROUTE)
public class SimulationOfferRestController {
    SimulationOfferService simulationOfferService;
    private final SimulationOfferMapper simulationOfferMapper;

    public SimulationOfferRestController(SimulationOfferService simulationOfferService,
                                         SimulationOfferMapper simulationOfferMapper) {
        this.simulationOfferService = simulationOfferService;
        this.simulationOfferMapper = simulationOfferMapper;
    }

    @PostMapping
    @ApiOperation(value = CALCULATE_AND_SHOW_PREMIUM_FOR_SIMULATION_OFFER, response = String.class)
    public String calculatePremium(@Valid @RequestBody SimulationOfferDTO simulationOfferDTO) {
        try {
            SimulationOffer simulationOffer = simulationOfferMapper.getSimulationOfferFromDTO(simulationOfferDTO);
            Double premium = simulationOffer.getPremium();
            return String.format(TOTAL_PREMIUM, premium);
        } catch (InvalidInputException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }
}