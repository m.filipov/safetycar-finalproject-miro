package com.telerikacademy.safetycar.controllers.rest;

public class RestControllerConstants {
    public static final String AGENT_REQUESTS_ROUTE = "/api/agent/requests";
    public static final String ID                   = "/{id}";
    public static final String FILTER_ROUTE         = "/filter";

    public static final String CITIES_ROUTE      = "/api/cities";
    public static final String MAKES_ROUTE       = "/api/makes";
    public static final String MODELS_ROUTE      = "/api/models";
    public static final String MAKE_ID_ROUTE     = "/make/{makeId}";
    public static final String REQUESTS          = "/api/requests";
    public static final String NEW_ROUTE         = "/new";
    public static final String FILE              = "file";
    public static final String OFFER             = "offer";
    public static final String POLICY            = "policy";
    public static final String OFFER_ROUTE       = "/api/offer";
    public static final String TOTAL_PREMIUM     = "Total premium: %.2f BGN";
    public static final String MY_REQUESTS_ROUTE = "/api/my-requests";
    public static final String USERS_ROUTE       = "/api/users";
}
