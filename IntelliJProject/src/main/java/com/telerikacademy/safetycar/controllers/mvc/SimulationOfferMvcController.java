package com.telerikacademy.safetycar.controllers.mvc;

import com.telerikacademy.safetycar.exceptions.InvalidInputException;
import com.telerikacademy.safetycar.models.SimulationOffer;
import com.telerikacademy.safetycar.models.dtos.SimulationOfferDTO;
import com.telerikacademy.safetycar.services.contracts.MakeService;
import com.telerikacademy.safetycar.services.contracts.ModelService;
import com.telerikacademy.safetycar.utils.SimulationOfferMapper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import static com.telerikacademy.safetycar.controllers.mvc.MvcControllerConstants.SimulationOfferMvc.*;

@Controller
@RequestMapping(OFFER_ROUTE)
public class SimulationOfferMvcController {
    private final MakeService           makeService;
    private final ModelService          modelService;
    private final SimulationOfferMapper simulationOfferMapper;

    public SimulationOfferMvcController(MakeService makeService,
                                        ModelService modelService,
                                        SimulationOfferMapper simulationOfferMapper) {
        this.makeService = makeService;
        this.modelService = modelService;
        this.simulationOfferMapper = simulationOfferMapper;
    }

    @GetMapping
    public String showCreateOfferPage(Model model) {
        model.addAttribute(SIMULATION_OFFER_DTO, new SimulationOfferDTO());
        return CREATE_SIMULATION_OFFER_HTML_PATH;
    }

    @PostMapping
    public String handleCreateOffer(@Valid @ModelAttribute(value = SIMULATION_OFFER_DTO) SimulationOfferDTO simulationOfferDTO,
                                    BindingResult bindingResult,
                                    Model model,
                                    HttpServletRequest request) {

        if (bindingResult.hasErrors()) {
            model.addAttribute(SIMULATION_OFFER_DTO, simulationOfferDTO);
            return CREATE_SIMULATION_OFFER_HTML_PATH;
        }
        try {
            SimulationOffer simulationOffer = simulationOfferMapper.getSimulationOfferFromDTO(simulationOfferDTO);
            HttpSession session = request.getSession();
            session.setAttribute(OFFER, simulationOffer);
            return CREATE_SIMULATION_OFFER_HTML_PATH;
        } catch (InvalidInputException e) {
            model.addAttribute(REGISTRATION_DATE_ERROR, e.getMessage());
            return CREATE_SIMULATION_OFFER_HTML_PATH;
        }
    }

    @ModelAttribute
    public void addAttributes(Model model) {
        model.addAttribute(MAKES, makeService.getAll());
        model.addAttribute(MODELS, modelService.getAll());
    }
}