package com.telerikacademy.safetycar.controllers.mvc;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import static com.telerikacademy.safetycar.controllers.mvc.MvcControllerConstants.LoginMvc.LOGIN_HTML_PATH;
import static com.telerikacademy.safetycar.controllers.mvc.MvcControllerConstants.LoginMvc.LOGIN_ROUTE;

@Controller
public class LoginMvcController {

    @GetMapping(LOGIN_ROUTE)
    public String showLoginPage() {
        return LOGIN_HTML_PATH;
    }
}