package com.telerikacademy.safetycar.exceptions;

public interface ExceptionsConstants {

    final class HandlerConstants {
        public static final String CONTROLLERS_ROUTE = "com.telerikacademy.safetycar.controllers.mvc";
        public static final String MESSAGE           = "message";
        public static final String ERROR             = "error";
        public static final String STATUS            = "status";
        public static final String REQUEST           = "Request ";

        public static final String LOG_MESSAGE_REQUEST         = "Request ";
        public static final String LOG_MESSAGE_THREW_EXCEPTION = " Threw an Exception";
        public static final String ERROR_404                   = "/errors/404";
        public static final String ACCESS_DENIED               = "/errors/access-denied";
        public static final String DUPLICATE_ERROR             = "/errors/duplicate-error";
        public static final String MAIL_NOT_SENT               = "/errors/mail-not-sent";
        public static final String FILE_STORAGE_ERROR          = "/errors/file-storage-error";
        public static final String INVALID_INPUT_ERROR         = "/errors/invalid-input-error";
        public static final String PASS_DONT_MATCH_ERROR       = "/errors/pass-dont-match-error";
        public static final String SAME_PASS_ERROR             = "/errors/same-pass-error";
        public static final String WRONG_PASS_ERROR            = "/errors/wrong-pass-error";
    }
}
