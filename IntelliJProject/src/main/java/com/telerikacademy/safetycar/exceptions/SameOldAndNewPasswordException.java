package com.telerikacademy.safetycar.exceptions;

public class SameOldAndNewPasswordException extends RuntimeException {
    public SameOldAndNewPasswordException(String message) {
        super(message);
    }
}