package com.telerikacademy.safetycar.exceptions.handler;

import com.telerikacademy.safetycar.exceptions.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

import static com.telerikacademy.safetycar.exceptions.ExceptionsConstants.HandlerConstants.*;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice(basePackages = "com.telerikacademy.safetycar.controllers.mvc")
public class CustomExceptionHandler {
    private final Log LOGGER = LogFactory.getLog(CustomExceptionHandler.class);

    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    @ExceptionHandler(value = {EntityNotFoundException.class})
    public ModelAndView handleEntityNotFound(EntityNotFoundException e) {
        ModelAndView mav = new ModelAndView();
        mav.setViewName(ERROR_404);
        mav.addObject(MESSAGE, e.getMessage());
        LOGGER.warn(e);
        return mav;
    }

    @ResponseStatus(value = HttpStatus.UNAUTHORIZED)
    @ExceptionHandler(value = {UnauthorizedOperationException.class})
    public ModelAndView handleEntityNotFound(UnauthorizedOperationException e) {
        ModelAndView mav = new ModelAndView();
        mav.setViewName(ACCESS_DENIED);
        mav.addObject(MESSAGE, e.getMessage());
        LOGGER.warn(e);
        return mav;
    }

    @ResponseStatus(value = HttpStatus.CONFLICT)
    @ExceptionHandler(value = {DuplicateEntityException.class})
    public ModelAndView handleDuplicateEntity(DuplicateEntityException e) {
        ModelAndView mav = new ModelAndView();
        mav.setViewName(DUPLICATE_ERROR);
        mav.addObject(MESSAGE, e.getMessage());
        LOGGER.warn(e);
        return mav;
    }

    @ResponseStatus(value = HttpStatus.EXPECTATION_FAILED)
    @ExceptionHandler(value = {EmailNotSentException.class})
    public ModelAndView handleEmailNotSent(EmailNotSentException e) {
        ModelAndView mav = new ModelAndView();
        mav.setViewName(MAIL_NOT_SENT);
        mav.addObject(MESSAGE, e.getMessage());
        LOGGER.warn(e);
        return mav;
    }

    @ResponseStatus(value = HttpStatus.CONFLICT)
    @ExceptionHandler(value = {FileStorageException.class})
    public ModelAndView handleFileStorageException(FileStorageException e) {
        ModelAndView mav = new ModelAndView();
        mav.setViewName(FILE_STORAGE_ERROR);
        mav.addObject(MESSAGE, e.getMessage());
        LOGGER.warn(e);
        return mav;
    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = {InvalidInputException.class})
    public ModelAndView handleInvalidInputException(InvalidInputException e) {
        ModelAndView mav = new ModelAndView();
        mav.setViewName(INVALID_INPUT_ERROR);
        mav.addObject(MESSAGE, e.getMessage());
        LOGGER.warn(e);
        return mav;
    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = {PasswordsDontMatchException.class})
    public ModelAndView handlePassDontMatchException(PasswordsDontMatchException e) {
        ModelAndView mav = new ModelAndView();
        mav.setViewName(PASS_DONT_MATCH_ERROR);
        mav.addObject(MESSAGE, e.getMessage());
        LOGGER.warn(e);
        return mav;
    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = {SameOldAndNewPasswordException.class})
    public ModelAndView handleSameOldAndNewPassException(SameOldAndNewPasswordException e) {
        ModelAndView mav = new ModelAndView();
        mav.setViewName(SAME_PASS_ERROR);
        mav.addObject(MESSAGE, e.getMessage());
        LOGGER.warn(e);
        return mav;
    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = {WrongPassWordException.class})
    public ModelAndView handleWrongPassException(WrongPassWordException e) {
        ModelAndView mav = new ModelAndView();
        mav.setViewName(WRONG_PASS_ERROR);
        mav.addObject(MESSAGE, e.getMessage());
        LOGGER.warn(e);
        return mav;
    }
}