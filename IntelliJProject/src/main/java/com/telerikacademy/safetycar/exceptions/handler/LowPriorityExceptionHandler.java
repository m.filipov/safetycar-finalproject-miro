package com.telerikacademy.safetycar.exceptions.handler;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.core.annotation.Order;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static com.telerikacademy.safetycar.exceptions.ExceptionsConstants.HandlerConstants.*;

@Order
@ControllerAdvice(basePackages = CONTROLLERS_ROUTE)
public class LowPriorityExceptionHandler {
    private final Log LOGGER = LogFactory.getLog(Exception.class);

    @ExceptionHandler(value = {Exception.class})
    public ModelAndView handleAllExceptions(HttpServletResponse response, HttpServletRequest request, Exception e) {
        ModelAndView mav = new ModelAndView();
        Integer statusCode = response.getStatus();
        mav.setViewName(ERROR);
        mav.addObject(MESSAGE, e.getMessage());
        mav.addObject(STATUS, statusCode);
        LOGGER.info(response.getStatus());
        LOGGER.error(LOG_MESSAGE_REQUEST + request.getRequestURL() + LOG_MESSAGE_THREW_EXCEPTION, e);
        return mav;
    }
}