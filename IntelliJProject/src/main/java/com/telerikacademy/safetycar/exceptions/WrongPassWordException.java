package com.telerikacademy.safetycar.exceptions;

public class WrongPassWordException extends RuntimeException {
    public WrongPassWordException(String message) {
        super(message);
    }
}