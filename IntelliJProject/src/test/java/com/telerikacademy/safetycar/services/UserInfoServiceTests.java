package com.telerikacademy.safetycar.services;

import com.telerikacademy.safetycar.exceptions.DuplicateEntityException;
import com.telerikacademy.safetycar.models.UserInfo;
import com.telerikacademy.safetycar.repositories.contracts.UserInfoRepository;
import com.telerikacademy.safetycar.services.impl.AuthorityServiceImpl;
import com.telerikacademy.safetycar.services.impl.UserInfoServiceImpl;
import com.telerikacademy.safetycar.services.impl.UserServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyString;

@ExtendWith(MockitoExtension.class)
public class UserInfoServiceTests {

    @InjectMocks
    UserInfoServiceImpl mockUserInfoService;
    AuthorityServiceImpl authorityService;
    UserServiceImpl      userService;

    @Mock
    UserInfoRepository mockUserInfoRepository;

//    @Mock
//    UserRepository mockUserRepository;
//
//    @Mock
//    AuthorityRepository mockAuthorityRepository;

    @Test
    public void getAll_ShouldReturn_EmptyListOfUsers() {
        // Arrange
        Mockito.when(mockUserInfoRepository.getAll())
                .thenReturn(Collections.emptyList());

        // Act
        List<UserInfo> users = mockUserInfoService.getAll();

        // Assert
        Assertions.assertEquals(0, users.size());
    }

    @Test
    public void getAll_ShouldReturn_ListOfUsers() {
        // Arrange
        List<UserInfo> list = new ArrayList<>();
        UserInfo userInfo = new UserInfo();
        list.add(userInfo);
        Mockito.when(mockUserInfoRepository.getAll())
                .thenReturn(list);

        // Act
        List<UserInfo> users = mockUserInfoService.getAll();

        // Assert
        Assertions.assertEquals(1, users.size());
    }

    @Test
    public void getById_ShouldReturn_WhenValidId() {
        // Arrange
        UserInfo searchedUserInfo = new UserInfo();
        Mockito.when(mockUserInfoRepository.getById(1))
                .thenReturn(searchedUserInfo);

        // Act
        UserInfo returnedUserInfo = mockUserInfoService.getById(1);

        // Assert
        Assertions.assertEquals(returnedUserInfo, searchedUserInfo);
    }

    @Test
    public void getByEmail_ShouldReturn_WhenValidEmail() {
        // Arrange
        UserInfo searchedUserInfo = new UserInfo();
        Mockito.when(mockUserInfoRepository.getByEmail(anyString()))
                .thenReturn(searchedUserInfo);

        // Act
        UserInfo returnedUserInfo = mockUserInfoService.getByEmail(anyString());

        // Assert
        Assertions.assertEquals(returnedUserInfo, searchedUserInfo);
    }

    @Test
    public void update_ShouldThrow_WhenEmailTaken() {
        // Arrange
        UserInfo userToUpdate = new UserInfo();
        Mockito.when(mockUserInfoRepository.existsByEmail(Mockito.any()))
                .thenReturn(true);

        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> mockUserInfoService.update(userToUpdate));
    }

    @Test
    public void update_ShouldReturn_WhenMailNotTaken() {
        // Arrange
        UserInfo userToUpdate = new UserInfo();
        Mockito.when(mockUserInfoRepository.existsByEmail(Mockito.any()))
                .thenReturn(false);

        // Act
        mockUserInfoService.update(userToUpdate);

        // Assert
        Mockito.verify(mockUserInfoRepository, Mockito.times(1))
                .update(userToUpdate);
    }

    @Test
    public void create_ShouldThrow_WhenEmailTaken() {
        // Arrange
        UserInfo userToCreate = new UserInfo();
        Mockito.when(mockUserInfoRepository.existsByEmail(Mockito.any()))
                .thenReturn(true);

        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class, () -> mockUserInfoService.create(userToCreate));
    }

    @Test
    public void create_ShouldReturn_WhenEmailNotTaken() {
        // Arrange
        UserInfo userToCreate = new UserInfo();
        Mockito.when(mockUserInfoRepository.existsByEmail(Mockito.any()))
                .thenReturn(false);

        // Act
        mockUserInfoService.create(userToCreate);

        // Assert
        Mockito.verify(mockUserInfoRepository, Mockito.times(1))
                .create(userToCreate);
    }
}