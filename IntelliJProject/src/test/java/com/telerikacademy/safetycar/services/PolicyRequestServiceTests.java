package com.telerikacademy.safetycar.services;

import com.telerikacademy.safetycar.exceptions.UnauthorizedOperationException;
import com.telerikacademy.safetycar.models.*;
import com.telerikacademy.safetycar.repositories.contracts.PolicyRequestRepository;
import com.telerikacademy.safetycar.services.contracts.StatusService;
import com.telerikacademy.safetycar.services.impl.PolicyRequestServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.*;

@ExtendWith(MockitoExtension.class)
public class PolicyRequestServiceTests {

    User isAdmin;
    User notAnAdmin;

    @InjectMocks
    PolicyRequestServiceImpl policyRequestService;

    @Mock
    StatusService mockStatusService;

    @Mock
    PolicyRequestRepository mockPolicyRequestRepository;

    @BeforeEach
    public void init() {
        isAdmin = new User();
        Set<Authority> roles = new HashSet<>();
        Authority authority = new Authority();
        roles.add(authority);
        authority.setUsername("test@test.test");
        authority.setAuthority("ROLE_ADMIN");
        isAdmin.setUsername("test@test.test");
        isAdmin.setAuthorities(roles);

        notAnAdmin = new User();
        roles = new HashSet<>();
        authority = new Authority();
        roles.add(authority);
        authority.setUsername("ivancho@mail.com");
        authority.setAuthority("ROLE_USER");
        notAnAdmin.setUsername("ivancho@mail.com");
        notAnAdmin.setAuthorities(roles);
    }

    @Test
    public void getAll_ShouldReturn_EmptyListOfPolicyRequests_WhenNoPolicies() {
        // Arrange
        Mockito.when(mockPolicyRequestRepository.getAll())
                .thenReturn(Collections.emptyList());

        // Act
        List<PolicyRequest> policies = policyRequestService.getAll();

        // Assert
        Assertions.assertEquals(0, policies.size());
    }

    @Test
    public void getAll_ShouldReturn_ListOfSimulationOffers() {
        // Arrange
        List<PolicyRequest> policyRequestsList = new ArrayList<>();
        PolicyRequest policyRequest = new PolicyRequest();
        policyRequestsList.add(policyRequest);
        Mockito.when(mockPolicyRequestRepository.getAll())
                .thenReturn(policyRequestsList);

        // Act
        List<PolicyRequest> policies = policyRequestService.getAll();

        // Assert
        Assertions.assertEquals(1, policies.size());
    }

    @Test
    public void getById_ShouldReturn_WhenValidId() {
        // Arrange
        PolicyRequest originalPolicy = new PolicyRequest();
        originalPolicy.setId(1);
        Mockito.when(mockPolicyRequestRepository.getById(1))
                .thenReturn(originalPolicy);

        // Act
        PolicyRequest returnedPolicy = policyRequestService.getById(1);

        // Assert
        Assertions.assertEquals(returnedPolicy, originalPolicy);
    }

    @Test
    public void getByCreator_ShouldReturn_ListOfPolicyRequests() {
        // Arrange
        List<PolicyRequest> policyRequestsList = new ArrayList<>();
        PolicyRequest policyRequest = new PolicyRequest();
        policyRequestsList.add(policyRequest);
        Mockito.when(mockPolicyRequestRepository.getByCreator(Mockito.anyInt()))
                .thenReturn(policyRequestsList);

        // Act
        List<PolicyRequest> returnedPolicies = policyRequestService.getByCreator(Mockito.anyInt());

        // Assert
        Assertions.assertEquals(1, returnedPolicies.size());
    }

    @Test
    public void getByCreator_ShouldReturn_EmptyList_WhenNoRequests() {
        // Arrange
        Mockito.when(mockPolicyRequestRepository.getByCreator(Mockito.anyInt()))
                .thenReturn(Collections.emptyList());

        // Act
        List<PolicyRequest> returnedPolicies = policyRequestService.getByCreator(Mockito.anyInt());

        // Assert
        Assertions.assertEquals(0, returnedPolicies.size());
    }

    @Test
    public void create_ShouldCreate_PolicyRequestAndSetPolicyNumber() {
        // Arrange
        PolicyRequest policyRequest = new PolicyRequest();

        // Act
        policyRequestService.create(policyRequest);

        //Assert
        Mockito.verify(mockPolicyRequestRepository,
                Mockito.times(1)).create(policyRequest);
        Mockito.verify(mockPolicyRequestRepository,
                Mockito.times(1)).update(policyRequest);
    }

    @Test
    public void update_ShouldUpdate_PolicyRequest() {
        // Arrange
        PolicyRequest policyRequest = new PolicyRequest();

        // Act
        policyRequestService.update(policyRequest);

        //Assert
        Mockito.verify(mockPolicyRequestRepository,
                Mockito.times(1)).update(policyRequest);
    }

    @Test
    public void cancel_ShouldUpdateStatus_WhenUserAuthorized() {
        // Arrange
        PolicyRequest policyRequest = new PolicyRequest();
        Status currentStatus = new Status();
        currentStatus.setStatusName("pending");
        policyRequest.setStatus(currentStatus);

        UserInfo testOwner = new UserInfo();
        testOwner.setEmail("test@test.test");
        policyRequest.setOwner(testOwner);

        User loggedUser = new User();
        loggedUser.setUsername("test@test.test");

        Status newStatus = new Status();
        newStatus.setStatusName("cancelled");
        Mockito.when(mockStatusService.getByStatusName(Mockito.anyString())).thenReturn(newStatus);

        // Act
        policyRequestService.cancel(policyRequest, loggedUser);

        // Assert
        Mockito.verify(mockPolicyRequestRepository,
                Mockito.times(1)).update(policyRequest);
    }

    @Test
    public void cancel_ShouldChangeStatus_WhenUserAuthorized() {
        // Arrange
        PolicyRequest policyRequest = new PolicyRequest();
        Status currentStatus = new Status();
        currentStatus.setStatusName("pending");
        policyRequest.setStatus(currentStatus);

        UserInfo testOwner = new UserInfo();
        testOwner.setEmail("test@test.test");
        policyRequest.setOwner(testOwner);

        User loggedUser = new User();
        loggedUser.setUsername("test@test.test");

        Status newStatus = new Status();
        newStatus.setStatusName("cancelled");
        Mockito.when(mockStatusService.getByStatusName(Mockito.anyString())).thenReturn(newStatus);

        // Act
        policyRequestService.cancel(policyRequest, loggedUser);

        // Assert
        Assertions.assertEquals(newStatus, policyRequest.getStatus());
    }

    @Test
    public void cancel_ShouldThrow_WhenUserNotOwner() {
        // Arrange
        PolicyRequest policyRequest = new PolicyRequest();
        Status currentStatus = new Status();
        currentStatus.setStatusName("pending");
        policyRequest.setStatus(currentStatus);

        UserInfo testOwner = new UserInfo();
        testOwner.setEmail("test@test.test");
        policyRequest.setOwner(testOwner);

        User loggedUser = new User();
        loggedUser.setUsername("other@test.test");

        Status newStatus = new Status();
        newStatus.setStatusName("cancelled");

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> policyRequestService.cancel(policyRequest, loggedUser));
    }

    @Test
    public void cancel_ShouldThrow_WhenPolicyNotPending() {
        // Arrange
        PolicyRequest policyRequest = new PolicyRequest();
        Status currentStatus = new Status();
        currentStatus.setStatusName("approved");
        policyRequest.setStatus(currentStatus);

        UserInfo testOwner = new UserInfo();
        testOwner.setEmail("test@test.test");
        policyRequest.setOwner(testOwner);

        User loggedUser = new User();
        loggedUser.setUsername("test@test.test");

        Status newStatus = new Status();
        newStatus.setStatusName("cancelled");

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> policyRequestService.cancel(policyRequest, loggedUser));
    }

    @Test
    public void approveOrDeny_ShouldUpdateStatus_WhenUserAuthorized() {
        // Arrange
        PolicyRequest policyRequest = new PolicyRequest();
        Status currentStatus = new Status();
        currentStatus.setStatusName("pending");
        policyRequest.setStatus(currentStatus);
        policyRequest.setEmail("policy@email.com");

        UserInfo testOwner = new UserInfo();
        testOwner.setEmail("owner@test.test");
        policyRequest.setOwner(testOwner);

        User loggedUser = isAdmin;
        loggedUser.setUsername("logged@test.test");

        Status newStatus = new Status();
        newStatus.setStatusName("approved");
        Mockito.when(mockStatusService.getByStatusName(Mockito.anyString())).thenReturn(newStatus);

        // Act
        policyRequestService.approveOrDeny(policyRequest, loggedUser, Mockito.anyString());

        // Assert
        Mockito.verify(mockPolicyRequestRepository,
                Mockito.times(1)).update(policyRequest);
    }

    @Test
    public void approveOrDeny_ShouldChangeStatus_WhenUserAuthorized() {
        // Arrange
        PolicyRequest policyRequest = new PolicyRequest();
        Status currentStatus = new Status();
        currentStatus.setStatusName("pending");
        policyRequest.setStatus(currentStatus);
        policyRequest.setEmail("policy@email.com");

        UserInfo testOwner = new UserInfo();
        testOwner.setEmail("owner@test.test");
        policyRequest.setOwner(testOwner);

        User loggedUser = isAdmin;
        loggedUser.setUsername("logged@test.test");

        Status newStatus = new Status();
        newStatus.setStatusName("approved");
        Mockito.when(mockStatusService.getByStatusName(Mockito.anyString())).thenReturn(newStatus);

        // Act
        policyRequestService.approveOrDeny(policyRequest, loggedUser, Mockito.anyString());

        // Assert
        Assertions.assertEquals(newStatus, policyRequest.getStatus());
    }

    @Test
    public void cancel_ShouldThrow_WhenUserIsOwner() {
        // Arrange
        PolicyRequest policyRequest = new PolicyRequest();
        Status currentStatus = new Status();
        currentStatus.setStatusName("pending");
        policyRequest.setStatus(currentStatus);
        policyRequest.setEmail("policy@email.com");

        UserInfo testOwner = new UserInfo();
        policyRequest.setOwner(testOwner);
        testOwner.setEmail("policy@email.com");

        User loggedUser = isAdmin;
        loggedUser.setUsername("policy@email.com");
        Status newStatus = new Status();
        newStatus.setStatusName("approved");

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> policyRequestService.approveOrDeny(policyRequest, loggedUser, "approved"));
    }

    @Test
    public void cancel_ShouldThrow_WhenUserIsNotAdmin() {
        // Arrange
        PolicyRequest policyRequest = new PolicyRequest();
        Status currentStatus = new Status();
        currentStatus.setStatusName("pending");
        policyRequest.setStatus(currentStatus);

        UserInfo testOwner = new UserInfo();
        testOwner.setEmail("test@test.test");
        policyRequest.setOwner(testOwner);

        User loggedUser = notAnAdmin;

        Status newStatus = new Status();
        newStatus.setStatusName("approved");

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> policyRequestService.approveOrDeny(policyRequest, loggedUser, "approved"));
    }

    @Test
    public void cancel_ShouldThrow_WhenPolicyCancelled() {
        // Arrange
        PolicyRequest policyRequest = new PolicyRequest();
        Status currentStatus = new Status();
        currentStatus.setStatusName("cancelled");
        policyRequest.setStatus(currentStatus);

        UserInfo testOwner = new UserInfo();
        testOwner.setEmail("other@test.test");
        policyRequest.setOwner(testOwner);

        User loggedUser = isAdmin;

        Status newStatus = new Status();
        newStatus.setStatusName("approved");

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> policyRequestService.approveOrDeny(policyRequest, loggedUser, "approved"));
    }

    @Test
    public void filter_ShouldReturn_ListOfFilteredPolicies() {
        // Arrange
        PolicyRequest policyRequest = new PolicyRequest();
        policyRequest.setFirstName("Test");
        List<PolicyRequest> list = new ArrayList<>();
        list.add(policyRequest);
        Mockito.when(mockPolicyRequestRepository.filter(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any()))
                .thenReturn(list);

        // Act
        List<PolicyRequest> returnedList = policyRequestService.filter(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any());

        // Assert
        Assertions.assertEquals(1, returnedList.size());
    }

    @Test
    public void filter_ShouldReturn_EmptyListOfFilteredPolicies() {
        // Arrange
        Mockito.when(mockPolicyRequestRepository.filter(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any()))
                .thenReturn(Collections.emptyList());

        // Act
        List<PolicyRequest> returnedList = policyRequestService.filter(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any());

        // Assert
        Assertions.assertEquals(0, returnedList.size());
    }
}