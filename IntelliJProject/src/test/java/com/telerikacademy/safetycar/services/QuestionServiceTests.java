package com.telerikacademy.safetycar.services;

import com.telerikacademy.safetycar.models.Question;
import com.telerikacademy.safetycar.repositories.contracts.QuestionRepository;
import com.telerikacademy.safetycar.services.impl.QuestionServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@ExtendWith(MockitoExtension.class)
public class QuestionServiceTests {

    @InjectMocks
    QuestionServiceImpl mockQuestionService;

    @Mock
    QuestionRepository mockQuestionRepository;

    @Test
    public void findAll_ShouldReturn_EmptyListOfQuestions() {
        // Arrange
        Mockito.when(mockQuestionRepository.findAll())
                .thenReturn(Collections.emptyList());

        // Act
        List<Question> questions = mockQuestionService.findAll();

        // Assert
        Assertions.assertEquals(0, questions.size());
    }

    @Test
    public void findAll_ShouldReturn_ListOfQuestions() {
        // Arrange
        List<Question> list = new ArrayList<>();
        Question question = new Question();
        list.add(question);
        Mockito.when(mockQuestionRepository.findAll())
                .thenReturn(list);

        // Act
        List<Question> questions = mockQuestionService.findAll();

        // Assert
        Assertions.assertEquals(1, questions.size());
    }

    @Test
    public void findById_ShouldReturn_WhenValidId() {
        // Arrange
        Question question = new Question();
        Mockito.when(mockQuestionRepository.findById(1))
                .thenReturn(question);

        // Act
        Question returnedQuestion = mockQuestionService.findById(1);

        // Assert
        Assertions.assertEquals(returnedQuestion, question);
    }
}