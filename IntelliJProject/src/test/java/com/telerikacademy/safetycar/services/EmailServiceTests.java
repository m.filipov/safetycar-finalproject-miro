package com.telerikacademy.safetycar.services;

import com.telerikacademy.safetycar.models.*;
import com.telerikacademy.safetycar.models.dtos.ContactDTO;
import com.telerikacademy.safetycar.repositories.contracts.VerificationTokenRepository;
import com.telerikacademy.safetycar.services.impl.EmailServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mail.javamail.JavaMailSender;
import org.thymeleaf.spring5.SpringTemplateEngine;

import javax.mail.Session;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

@ExtendWith(MockitoExtension.class)
@RunWith(MockitoJUnitRunner.class)
public class EmailServiceTests {

    @InjectMocks
    EmailServiceImpl emailService;

    @Mock
    VerificationTokenRepository mockVerificationTokenRepository;
    @Spy
    SpringTemplateEngine mockTemplateEngine;
    @Mock
    JavaMailSender mockJavaMailSender;

    @Test
    public void sendHtmlMail() {
        // Arrange
        User user = new User();
        user.setUsername("testUsername@test.test");

        VerificationToken verificationToken = new VerificationToken();
        verificationToken.setUser(user);
        verificationToken.setToken("testToken");

        //get the token
        Mockito.when(mockVerificationTokenRepository.findByUser(user)).thenReturn(verificationToken);

        //creating MimeMessage
        Properties properties = System.getProperties();
        Session session = Session.getDefaultInstance(properties);
        MimeMessage mimeMessage = new MimeMessage(session);

        Mockito.when(mockJavaMailSender.createMimeMessage()).thenReturn(mimeMessage);

        // Act
        emailService.sendRegistrationMail(user);

        // Assert
        Mockito.verify(mockJavaMailSender, Mockito.times(1)).send(mimeMessage);
    }

    @Test
    public void statusChangeEmail_ShouldReturn_WhenValidMail() {
        // Arrange

        PolicyRequest policyRequest = new PolicyRequest();
        Status status = new Status();
        status.setStatusName("pending");

        UserInfo userInfo = new UserInfo();
        userInfo.setEmail("testUsername@test.test");

        policyRequest.setStatus(status);
        policyRequest.setOwner(userInfo);
        policyRequest.setEmail(userInfo.getEmail());

        //creating MimeMessage
        Properties properties = System.getProperties();
        Session session = Session.getDefaultInstance(properties);
        MimeMessage mimeMessage = new MimeMessage(session);

        Mockito.when(mockJavaMailSender.createMimeMessage()).thenReturn(mimeMessage);

        // Act
        emailService.sendStatusChangeEmail(policyRequest, policyRequest.getOwner().getEmail());

        // Assert
        Mockito.verify(mockJavaMailSender, Mockito.times(1)).send(mimeMessage);
    }

    @Test
    public void statusChangeEmail_ShouldSendSecondMail_WhenUsersDiffer() {
        // Arrange

        PolicyRequest policyRequest = new PolicyRequest();
        Status status = new Status();
        status.setStatusName("pending");

        UserInfo userInfo = new UserInfo();
        userInfo.setEmail("testUsername@test.test");

        policyRequest.setStatus(status);
        policyRequest.setOwner(userInfo);
        String emailOwner = "email@mail.com";
        policyRequest.setEmail(emailOwner);

        //creating MimeMessage
        Properties properties = System.getProperties();
        Session session = Session.getDefaultInstance(properties);
        MimeMessage mimeMessage = new MimeMessage(session);

        Mockito.when(mockJavaMailSender.createMimeMessage()).thenReturn(mimeMessage);

        // Act
        emailService.sendStatusChangeEmail(policyRequest, emailOwner);

        // Assert
        Mockito.verify(mockJavaMailSender, Mockito.times(2)).send(mimeMessage);
    }

    @Test
    public void contactEmail_ShouldSendMessage() {
        // Arrange

        ContactDTO dto = new ContactDTO();
        dto.setEmail("testEmail@test.test");
        dto.setMessage("message");
        dto.setSubject("subject");
        dto.setName("Test Testov");

        //creating MimeMessage
        Properties properties = System.getProperties();
        Session session = Session.getDefaultInstance(properties);
        MimeMessage mimeMessage = new MimeMessage(session);

        Mockito.when(mockJavaMailSender.createMimeMessage()).thenReturn(mimeMessage);

        // Act
        emailService.contactEmail(dto);

        // Assert
        Mockito.verify(mockJavaMailSender, Mockito.times(1)).send(mimeMessage);
    }

    @Test
    public void sendResetPasswordEmail_ShouldSendEmail() {
        // Arrange
        User user = new User();
        user.setUsername("testUsername@test.test");

        VerificationToken verificationToken = new VerificationToken();
        verificationToken.setUser(user);
        verificationToken.setToken("testToken");

        //get the token
        Mockito.when(mockVerificationTokenRepository.findByToken(verificationToken.getToken()))
                .thenReturn(verificationToken);

        //creating MimeMessage
        Properties properties = System.getProperties();
        Session session = Session.getDefaultInstance(properties);
        MimeMessage mimeMessage = new MimeMessage(session);

        Mockito.when(mockJavaMailSender.createMimeMessage()).thenReturn(mimeMessage);

        // Act
        emailService.sendResetPasswordEmail(user, verificationToken.getToken());

        // Assert
        Mockito.verify(mockJavaMailSender, Mockito.times(1)).send(mimeMessage);
    }
}