package com.telerikacademy.safetycar.services;

import com.telerikacademy.safetycar.models.Model;
import com.telerikacademy.safetycar.models.SimulationOffer;
import com.telerikacademy.safetycar.repositories.contracts.SimulationOfferRepository;
import com.telerikacademy.safetycar.services.impl.SimulationOfferServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@ExtendWith(MockitoExtension.class)
public class SimulationOfferServiceTests {

    @InjectMocks
    SimulationOfferServiceImpl simulationOfferService;

    @Mock
    SimulationOfferRepository mockSimulationOfferRepository;

    @Test
    public void getAll_ShouldReturn_EmptyListOfSimulationOffers() {
        // Arrange
        Mockito.when(mockSimulationOfferRepository.getAll())
                .thenReturn(Collections.emptyList());

        // Act
        List<SimulationOffer> offers = simulationOfferService.getAll();

        // Assert
        Assertions.assertEquals(0, offers.size());
    }

    @Test
    public void getAll_ShouldReturn_ListOfSimulationOffers() {
        // Arrange
        List<SimulationOffer> simulationOfferList = new ArrayList<>();
        SimulationOffer simulationOffer = new SimulationOffer();
        simulationOfferList.add(simulationOffer);
        Mockito.when(mockSimulationOfferRepository.getAll())
                .thenReturn(simulationOfferList);

        // Act
        List<SimulationOffer> offers = simulationOfferService.getAll();

        // Assert
        Assertions.assertEquals(1, offers.size());
    }

    @Test
    public void getById_ShouldReturn_WhenValidId() {
        // Arrange
        SimulationOffer originalOffer = new SimulationOffer();
        originalOffer.setId(1);
        Mockito.when(mockSimulationOfferRepository.getById(1))
                .thenReturn(originalOffer);

        // Act
        SimulationOffer returnedOffer = simulationOfferService.getById(1);

        // Assert
        Assertions.assertEquals(returnedOffer, originalOffer);
    }

    @Test
    public void create_ShouldCreate_SimulationOffer() {
        // Arrange
        SimulationOffer simulationOffer = new SimulationOffer();

        // Act
        simulationOfferService.create(simulationOffer);

        //Assert
        Mockito.verify(mockSimulationOfferRepository,
                Mockito.times(1)).create(simulationOffer);
    }

    @Test
    public void update_ShouldUpdate_SimulationOffer() {
        // Arrange
        SimulationOffer simulationOffer = new SimulationOffer();

        // Act
        simulationOfferService.update(simulationOffer);

        //Assert
        Mockito.verify(mockSimulationOfferRepository,
                Mockito.times(1)).update(simulationOffer);
    }

    @Test
    public void calculatePremium_ShouldReturn_Premium() {
        // Arrange
        SimulationOffer simulationOffer = new SimulationOffer();
        simulationOffer.setId(1);
        Model model = new Model();
        simulationOffer.setModel(model);
        simulationOffer.setCubicCapacity(1000);
        simulationOffer.setFirstRegistrationDate("2020-10-20");
        simulationOffer.setDriverAge(20);
        simulationOffer.setPrevYearAccidents(true);
        Mockito.when(mockSimulationOfferRepository.getBaseAmountFromCsv(simulationOffer)).thenReturn(403.25);

        // Act
        Double calculatedPremium = simulationOfferService.calculatePremium(simulationOffer);

        //Assert
        Assertions.assertEquals(558.9045, calculatedPremium);
    }
}