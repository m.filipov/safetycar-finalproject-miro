package com.telerikacademy.safetycar.services;

import com.telerikacademy.safetycar.exceptions.UnauthorizedOperationException;
import com.telerikacademy.safetycar.models.Authority;
import com.telerikacademy.safetycar.models.User;
import com.telerikacademy.safetycar.repositories.contracts.UserRepository;
import com.telerikacademy.safetycar.services.impl.UserServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashSet;
import java.util.Set;

import static org.mockito.ArgumentMatchers.anyString;

@ExtendWith(MockitoExtension.class)
public class UserServiceTests {

    User isAdmin;
    User notAnAdmin;

    @InjectMocks
    UserServiceImpl mockUserService;

    @Mock
    UserRepository mockUserRepository;

    @BeforeEach
    public void init() {
        isAdmin = new User();
        Set<Authority> roles = new HashSet<>();
        Authority authority = new Authority();
        roles.add(authority);
        authority.setUsername("ivan@mail.com");
        authority.setAuthority("ROLE_ADMIN");
        isAdmin.setUsername("ivan@mail.com");
        isAdmin.setAuthorities(roles);

        notAnAdmin = new User();
        roles = new HashSet<>();
        authority = new Authority();
        roles.add(authority);
        authority.setUsername("ivancho@mail.com");
        authority.setAuthority("ROLE_USER");
        notAnAdmin.setUsername("ivancho@mail.com");
        notAnAdmin.setAuthorities(roles);
    }

    @Test
    public void update_ShouldThrow_WhenNotAdmin() {
        // Arrange, Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> mockUserService.update(isAdmin, notAnAdmin));
    }

    @Test
    public void update_ShouldReturn_WhenSameUser() {
        // Arrange, Act
        mockUserService.update(notAnAdmin, notAnAdmin);

        // Assert
        Mockito.verify(mockUserRepository, Mockito.times(1)).update(notAnAdmin);
    }

    @Test
    public void update_ShouldReturn_WhenAdmin() {
        // Arrange, Act
        mockUserService.update(notAnAdmin, isAdmin);

        // Assert
        Mockito.verify(mockUserRepository, Mockito.times(1)).update(notAnAdmin);
    }

    @Test
    public void delete_ShouldThrow_WhenNotAdmin() {
        // Arrange, Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> mockUserService.delete(isAdmin, notAnAdmin));
    }

    @Test
    public void delete_ShouldReturn_WhenAdmin() {
        // Arrange, Act
        mockUserService.delete(notAnAdmin, isAdmin);

        // Assert
        Mockito.verify(mockUserRepository, Mockito.times(1)).delete(notAnAdmin);
    }

    @Test
    public void getByEmail_ShouldReturn_WhenValidEmail() {
        // Arrange
        User originalUser = new User();
        Mockito.when(mockUserRepository.getByEmail(anyString()))
                .thenReturn(originalUser);

        // Act
        User returnedUser = mockUserService.getByEmail(anyString());

        // Assert
        Assertions.assertEquals(returnedUser, originalUser);
    }

    @Test
    public void resetPass_ShouldReturn_WhenCalled() {
        // Arrange
        User user = new User();
        Mockito.doNothing().when(mockUserRepository).update(user);

        // Act
        mockUserService.resetPassword(user);

        // Assert
        Mockito.verify(mockUserRepository, Mockito.times(1)).update(user);
    }
}