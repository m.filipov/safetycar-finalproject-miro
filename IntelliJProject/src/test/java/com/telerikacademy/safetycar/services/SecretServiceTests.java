package com.telerikacademy.safetycar.services;

import com.telerikacademy.safetycar.models.Question;
import com.telerikacademy.safetycar.models.Secret;
import com.telerikacademy.safetycar.models.User;
import com.telerikacademy.safetycar.models.UserInfo;
import com.telerikacademy.safetycar.repositories.contracts.SecretRepository;
import com.telerikacademy.safetycar.services.impl.SecretServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

@ExtendWith(MockitoExtension.class)
public class SecretServiceTests {

    @InjectMocks
    SecretServiceImpl mockSecretService;

    @Mock
    SecretRepository mockSecretRepository;

    @Test
    public void findByAnswer_Should_Return_WhenAnswerExists() {
        // Arrange
        Secret searchedSecret = new Secret();
        Mockito.when(mockSecretRepository.findByAnswer(Mockito.anyString()))
                .thenReturn(searchedSecret);

        // Act
        Secret returnedSecret = mockSecretService.findByAnswer(Mockito.anyString());

        // Assert
        Assertions.assertEquals(returnedSecret, searchedSecret);
    }

    @Test
    public void findByUser_ShouldReturn_WhenUserExists() {
        // Arrange
        List<Secret> secret = new ArrayList<>();
        User user = new User();
        Mockito.when(mockSecretRepository.findByUser(user))
                .thenReturn(secret);

        // Act
        List<Secret> returnedSecret = mockSecretService.findByUser(user);

        // Assert
        Assertions.assertEquals(returnedSecret, secret);
    }

    @Test
    public void save_ShouldReturn_WhenCalled() {
        // Arrange
        UserInfo userInfo = new UserInfo();
        Secret secret = new Secret();
        Question question = new Question();
        question.setName("question");
        String answer = "answer";
        secret.setAnswer(answer);
        secret.setUser(userInfo);

        // Act
        mockSecretService.save(userInfo, question, answer);

        // Assert
        Mockito.verify(mockSecretRepository, Mockito.times(1))
                .save(Mockito.any());
    }
}