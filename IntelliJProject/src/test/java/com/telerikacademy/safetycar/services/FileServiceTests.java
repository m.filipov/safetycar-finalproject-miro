package com.telerikacademy.safetycar.services;

import com.telerikacademy.safetycar.exceptions.FileStorageException;
import com.telerikacademy.safetycar.repositories.impl.CloudStorageRepositoryImpl;
import com.telerikacademy.safetycar.services.impl.FileServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mock.web.MockMultipartFile;

import java.io.IOException;

@ExtendWith(MockitoExtension.class)
public class FileServiceTests {

    @Mock
    CloudStorageRepositoryImpl mockCloudStorageRepository;

    @InjectMocks
    FileServiceImpl mockService;

    @Test
    public void uploadFileShould_ReturnUrl() throws IOException {
        // Arrange
        String expected = "url";
        MockMultipartFile multipartFile = new MockMultipartFile("name", "test.png", "image/png", "content".getBytes());
        Mockito.when(mockCloudStorageRepository.uploadFile("content".getBytes(), "image", 300)).thenReturn(expected);

        // Act
        String actual = mockService.uploadFile(multipartFile, 300);

        // Assert
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void uploadFileShould_Throw_WhenNoFileUploaded() {
        // Arrange, Act, Assert
        Assertions.assertThrows(FileStorageException.class,
                () -> mockService.uploadFile(null, 0));
    }

    @Test
    public void getFileTypeShould_ReturnFileType() {
        // Arrange, Act, Assert
        Assertions.assertEquals("image", mockService.getFileType(new MockMultipartFile("name", "test.png", "image/png", "content".getBytes())));
    }
}