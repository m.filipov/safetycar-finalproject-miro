package com.telerikacademy.safetycar.services;

import com.telerikacademy.safetycar.models.Authority;
import com.telerikacademy.safetycar.repositories.contracts.AuthorityRepository;
import com.telerikacademy.safetycar.services.impl.AuthorityServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.telerikacademy.safetycar.utils.Constants.ROLE_ADMIN;
import static com.telerikacademy.safetycar.utils.Constants.TEST_EMAIL;

@ExtendWith(MockitoExtension.class)
public class AuthorityServiceTests {

    @InjectMocks
    AuthorityServiceImpl authorityService;

    @Mock
    AuthorityRepository mockAuthorityRepository;

    @Test
    public void create_ShouldReturn_Called() {
        // Arrange
        Authority authority = new Authority();
        authority.setAuthority(ROLE_ADMIN);
        authority.setUsername(TEST_EMAIL);

        // Act
        authorityService.create(authority);

        // Assert
        Mockito.verify(mockAuthorityRepository, Mockito.times(1))
                .create(authority);
    }

    @Test
    public void delete_ShouldReturn_Called() {
        // Arrange
        Authority authority = new Authority();
        authority.setAuthority(ROLE_ADMIN);
        authority.setUsername(TEST_EMAIL);

        // Act
        authorityService.delete(authority);

        // Assert
        Mockito.verify(mockAuthorityRepository, Mockito.times(1)).delete(authority);
    }
}